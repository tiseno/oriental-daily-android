package oriental.orientalOnePaper;

import android.app.Activity;
import android.content.Context;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CustomAdapter_ParticularCategoryAllNews extends BaseAdapter {

	private Activity activity;
	private String[] title, date, imagepath;
	private static LayoutInflater inflater = null;
	private ImageLoader_Loader imageLoader;
	private WindowManager wm = null;
	private Display display;

	public CustomAdapter_ParticularCategoryAllNews(Activity a, String[] title,
			String[] date, String[] imagepath) {
		activity = a;
		this.title = title;
		this.date = date;
		this.imagepath = imagepath;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
		imageLoader = new ImageLoader_Loader(activity.getApplicationContext());
	}

	public int getCount() {
		return title.length;
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public void setTitle(String[] title) {
		this.title = title;
	}

	public void setDate(String[] date) {
		this.date = date;
	}

	public void setImagepath(String[] imagepath) {
		this.imagepath = imagepath;
	}

	@SuppressWarnings("deprecation")
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.main_particularcategoryallnewslist,
					parent, false);

		LinearLayout linear = (LinearLayout) vi.findViewById(R.id.layout_image);

		ImageView imageview = (ImageView) vi
				.findViewById(R.id.image_categoryallnewstitle);

		TextView titletext = (TextView) vi
				.findViewById(R.id.text_categoryallnewstitle);

		TextView datetext = (TextView) vi.findViewById(R.id.text_newsdate);

		if (!imagepath[position].toString().equals("no picture")) {
			imageview.setVisibility(View.VISIBLE);
			linear.setVisibility(View.VISIBLE);
			imageLoader.DisplayImage(imagepath[position], imageview);
		} else {
			imageview.setVisibility(View.GONE);
			imageview.setImageDrawable(null);
			linear.setVisibility(View.GONE);
			display = wm.getDefaultDisplay();
			int screenWidth = display.getWidth();
			titletext.setWidth(screenWidth);
		}
		titletext.setText(title[position].toString());
		datetext.setText(date[position].toString());
		return vi;
	}
}
