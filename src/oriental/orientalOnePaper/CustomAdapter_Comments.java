package oriental.orientalOnePaper;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CustomAdapter_Comments extends BaseAdapter{

	private Activity activity;
	private String[] commentposter, comment, commentdate;
	private static LayoutInflater inflater = null;
	
	public CustomAdapter_Comments(Activity activity, String[] comment, String[] commentposter, String[] commentdate){
		this.activity = activity;
		this.comment = comment;
		this.commentposter = commentposter;
		this.commentdate = commentdate;
		inflater = (LayoutInflater) this.activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public int getCount() {
		return comment.length;
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.main_newscommentslist, parent,
					false);
		
		TextView comments = (TextView) vi.findViewById(R.id.text_comment);
		comments.setText(comment[position]);
		
		TextView commentsposter = (TextView) vi.findViewById(R.id.text_commentposter);
		commentsposter.setText(commentposter[position]);
		
		TextView commentsdate = (TextView) vi.findViewById(R.id.text_commentdate);
		commentsdate.setText(commentdate[position]);
		return vi;
	}

}
