package oriental.orientalOnePaper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapter_AllCategoryNews extends BaseAdapter {

	private Activity activity;
	private String[] category, temp = { Config_ConstantVariable.main }, string;
	private static LayoutInflater inflater = null;
	private int[] imagepath = { R.color.button_changeicon10,
			R.color.button_changeicon0, R.color.button_changeicon1,
			R.color.button_changeicon2, R.color.button_changeicon3,
			R.color.button_changeicon4, R.color.button_changeicon5,
			R.color.button_changeicon6, R.color.button_changeicon7,
			R.color.button_changeicon8, R.color.button_changeicon9 },
			isselected = { R.color.button_changeicon10_active,
					R.color.button_changeicon0_active,
					R.color.button_changeicon1_active,
					R.color.button_changeicon2_active,
					R.color.button_changeicon3_active,
					R.color.button_changeicon4_active,
					R.color.button_changeicon5_active,
					R.color.button_changeicon6_active,
					R.color.button_changeicon7_active,
					R.color.button_changeicon8_active,
					R.color.button_changeicon9_active };
	private int[] colordefault = { Color.argb(99, 178, 178, 178),
			Color.argb(99, 178, 178, 178), Color.argb(99, 178, 178, 178),
			Color.argb(99, 178, 178, 178), Color.argb(99, 178, 178, 178),
			Color.argb(99, 178, 178, 178), Color.argb(99, 178, 178, 178),
			Color.argb(99, 178, 178, 178), Color.argb(99, 178, 178, 178),
			Color.argb(99, 178, 178, 178), Color.argb(99, 178, 178, 178) },
			colorselected = { Color.argb(99, 198, 12, 12),
					Color.argb(99, 198, 12, 12), Color.argb(99, 198, 12, 12),
					Color.argb(99, 198, 12, 12), Color.argb(99, 198, 12, 12),
					Color.argb(99, 198, 12, 12), Color.argb(99, 198, 12, 12),
					Color.argb(99, 198, 12, 12), Color.argb(99, 198, 12, 12),
					Color.argb(99, 198, 12, 12), Color.argb(99, 198, 12, 12) };
	private int selectedpos, total;
	private boolean isthispage;

	public CustomAdapter_AllCategoryNews(Activity a, String[] category,
			int selectedpos, boolean isthispage) {
		activity = a;
		this.category = category;
		this.isthispage = isthispage;
		this.selectedpos = selectedpos;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return imagepath.length;
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		total = temp.length + category.length;
		string = new String[total];
		for (int i = 0; i < temp.length; i++) {
			string[i] = temp[i];
		}
		for (int i = 0; i < category.length; i++) {
			string[temp.length + i] = category[i];
		}
		if (convertView == null)
			vi = inflater.inflate(R.layout.main_allcategorynewslist, parent,
					false);
		if (this.isthispage && selectedpos > -1) {
			imagepath[selectedpos] = isselected[selectedpos];
			colordefault[selectedpos] = colorselected[selectedpos];
		}

		ImageView imageview = (ImageView) vi.findViewById(R.id.image_category);
		TextView categorytext = (TextView) vi.findViewById(R.id.text_category);
		try {
			imageview.setImageResource(imagepath[position]);
			categorytext.setText(string[position].toString());
			categorytext.setTextColor(colordefault[position]);
		} catch (Exception ex) {

		}
		return vi;
	}
}
