package oriental.orientalOnePaper;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Database_DataBaseHelper extends SQLiteOpenHelper {

	private static final String dbName = "demoDB1";

	/* ========================================== */
	private static final String NewsCategoryTable = "tblCatNews";
	private static final String colNewsCatinid = "id";
	private static final String colNewsCatid = "News_Cat_ID";
	private static final String colNewsCatNames = "News_Cat_Name";

	private static final String NewsTable = "tblNews";
	private static final String colNewsinid = "id";
	private static final String colNewsid = "News_id";
	private static final String colTitle = "News_Title";
	private static final String colIntroText = "News_IntroText";
	private static final String colFullText = "News_FullText";
	private static final String colArticalDate = "News_ArticalDate";
	private static final String colImagePath = "News_ImagePath";
	private static final String colNewsCat = "News_Cat_ID";
	private static final String colNewsAuthor = "News_Author";

	static final String SurveyFormTable = "tblSurveyForm";
	static final String colFormid = "Form_id";
	static final String colFormUserName = "UserName";
	static final String colFormUserEmail = "UserEmail";
	static final String colFormSubmit = "SurveyForm_Submit";
	static final String colnotification = "notificationsetting";
	static final String colsound = "notificationsound";
	static final String colvibrate = "notificationvibrate";
	static final String colpopup = "notificationpopup";
	static final String colregid = "RegistrationId";

	static final String AboutUsTable = "tblAboutUs";
	static final String colAboutid = "About_id";
	static final String colAboutImagePath = "About_ImagePaht";
	static final String colAboutLink = "About_Link";
	static final String colAboutDate = "About_Date";

	static final String FontSettingTable = "tblFontSetting";
	static final String colFontid = "FontSetting_id";
	static final String colFontsize = "FontSetting_size";

	/* ============================================= */

	public Database_DataBaseHelper(Context context) {
		super(context, dbName, null, 500);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL("CREATE TABLE " + NewsCategoryTable + "(" + colNewsCatinid
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + colNewsCatid
				+ " INTEGER, " + colNewsCatNames + " TEXT )");

		db.execSQL("CREATE TABLE " + NewsTable + "(" + colNewsinid
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + colNewsid
				+ " INTEGER, " + colNewsCat + " INTEGER, " + colTitle
				+ " TEXT, " + colIntroText + " TEXT, " + colFullText
				+ " TEXT, " + colArticalDate + " TEXT, " + colImagePath
				+ " TEXT, " + colNewsAuthor + " TEXT )");

		db.execSQL("CREATE TABLE " + SurveyFormTable + "(" + colFormid
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + colFormSubmit
				+ " INTEGER, " + colnotification + " INTEGER, " + colsound
				+ " INTEGER, " + colvibrate + " INTEGER, " + colpopup
				+ " INTEGER, " + colregid + " TEXT )");

		db.execSQL("CREATE TABLE " + AboutUsTable + "(" + colAboutid
				+ " INTEGER PRIMARY KEY, " + colAboutImagePath + " TEXT, "
				+ colAboutDate + " TEXT, " + colAboutLink + " TEXT )");

		db.execSQL("CREATE TABLE " + FontSettingTable + "(" + colFontid
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + colFontsize
				+ " INTEGER )");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + NewsCategoryTable);
		db.execSQL("DROP TABLE IF EXISTS " + NewsTable);
		db.execSQL("DROP TABLE IF EXISTS " + SurveyFormTable);
		db.execSQL("DROP TABLE IF EXISTS " + AboutUsTable);
		db.execSQL("DROP TABLE IF EXISTS " + FontSettingTable);

		onCreate(db);
	}

	/* ================Category News==================== */
	public void AddCatNews(List_CategoryNews NewsCats) {
		SQLiteDatabase Catdb = this.getWritableDatabase();

		ContentValues Cats = new ContentValues();
		Cats.put(colNewsCatid, NewsCats.getCatID());
		Cats.put(colNewsCatNames, NewsCats.getNewCatName());

		Catdb.insert(NewsCategoryTable, null, Cats);
		Catdb.close();
	}

	public List<List_CategoryNews> getAllNewsCategories() {
		List<List_CategoryNews> newNewsCat = new ArrayList<List_CategoryNews>();
		String selectQuery = "SELECT * FROM " + NewsCategoryTable + " WHERE "
				+ colNewsCatid + " != 10";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery(selectQuery, null);

		if (cur.moveToFirst()) {
			do {
				List_CategoryNews Catnews = new List_CategoryNews();

				Catnews.SetCatID(Integer.parseInt(cur.getString(1)));
				Catnews.setNewsCatName(cur.getString(2));

				newNewsCat.add(Catnews);
			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		return newNewsCat;
	}

	public void DeleteNewsCat(List_CategoryNews NewCatID) {
		SQLiteDatabase db = this.getWritableDatabase();

		db.delete(NewsCategoryTable, colNewsCatid + "=?",
				new String[] { String.valueOf(NewCatID.getCatID()) });
		db.close();
	}

	public void DeleteNewsCatFT() {
		SQLiteDatabase db = this.getWritableDatabase();

		db.delete(NewsCategoryTable, null, null);
		db.close();
	}

	/* ================News==================== */
	public void AddNews(List_News addNews) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues cv = new ContentValues();
		cv.put(colNewsid, addNews.getID());
		cv.put(colNewsCatid, addNews.getCatid());
		cv.put(colTitle, addNews.getNtitle());
		cv.put(colIntroText, addNews.getNintrotext());
		cv.put(colFullText, addNews.getFullText());
		cv.put(colArticalDate, addNews.getNArticalD());
		cv.put(colImagePath, addNews.getImagePath());
		cv.put(colNewsAuthor, addNews.getAuthor());
		db.insert(NewsTable, null, cv);
		db.close();
	}

	public List<List_News> getAllNews() {
		List<List_News> newNews = new ArrayList<List_News>();
		String selectQuery = "SELECT * FROM " + NewsTable + " ORDER BY "
				+ colNewsid + " DESC ";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery(selectQuery, null);

		if (cur.moveToFirst()) {
			do {
				List_News news = new List_News();
				news.SetID(Integer.parseInt(cur.getString(1)));
				news.SetCatID(Integer.parseInt(cur.getString(2)));
				news.setNtitle(cur.getString(3));
				news.setNintroText(cur.getString(4));
				news.setFullText(cur.getString(5));
				news.setNArtical(cur.getString(6));
				news.setImagePaht(cur.getString(7));
				news.setAuthor(cur.getString(8));

				newNews.add(news);
			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		return newNews;
	}

	public List<List_News> getAllTodaysNews() {
		List<List_News> newNews = new ArrayList<List_News>();

		String selectQuery = "SELECT * FROM " + NewsTable + " LEFT JOIN "
				+ NewsCategoryTable + " ON " + NewsTable + "." + colNewsCat
				+ "=" + NewsCategoryTable + "." + colNewsCatid + " ORDER BY "
				+ NewsTable + "." + colNewsid + " DESC limit 10 ";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery(selectQuery, null);

		if (cur.moveToFirst()) {
			do {
				List_News news = new List_News();
				news.SetID(Integer.parseInt(cur.getString(1)));
				news.SetCatID(Integer.parseInt(cur.getString(2)));
				news.setNtitle(cur.getString(3));
				news.setNintroText(cur.getString(4));
				news.setFullText(cur.getString(5));
				news.setNArtical(cur.getString(6));
				news.setImagePaht(cur.getString(7));
				news.setAuthor(cur.getString(8));
				news.SetCatIDs(Integer.parseInt(cur.getString(10)));
				news.setNewsCatName(cur.getString(11));

				newNews.add(news);

			} while (cur.moveToNext());
			cur.close();
		}

		db.close();

		return newNews;
	}

	public List<List_News> getSingleCatNews(int NewsCatID) {
		List<List_News> newNews = new ArrayList<List_News>();

		String selectQuery = "SELECT * FROM " + NewsTable + " LEFT JOIN "
				+ NewsCategoryTable + " ON " + NewsTable + "." + colNewsCat
				+ "=" + NewsCategoryTable + "." + colNewsCatid + " WHERE "
				+ NewsTable + "." + colNewsCat + " = " + NewsCatID
				+ " ORDER BY " + NewsTable + "." + colNewsid + " DESC ";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery(selectQuery, null);

		if (cur.moveToFirst()) {
			do {
				List_News news = new List_News();
				news.SetID(Integer.parseInt(cur.getString(1)));
				news.SetCatID(Integer.parseInt(cur.getString(2)));
				news.setNtitle(cur.getString(3));
				news.setNintroText(cur.getString(4));
				news.setFullText(cur.getString(5));
				news.setNArtical(cur.getString(6));
				news.setImagePaht(cur.getString(7));
				news.setAuthor(cur.getString(8));
				news.SetCatIDs(Integer.parseInt(cur.getString(10)));
				news.setNewsCatName(cur.getString(11));

				newNews.add(news);

			} while (cur.moveToNext());
			cur.close();
		}

		db.close();

		return newNews;
	}

	public List<List_News> getSingleNews(int getNews) {
		List<List_News> newNews = new ArrayList<List_News>();

		String selectQuery = "SELECT * FROM " + NewsTable + " LEFT JOIN "
				+ NewsCategoryTable + " ON " + NewsTable + "." + colNewsCat
				+ "=" + NewsCategoryTable + "." + colNewsCatid + " WHERE "
				+ NewsTable + "." + colNewsid + " = " + getNews;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery(selectQuery, null);

		if (cur.moveToFirst()) {
			do {
				List_News news = new List_News();
				news.SetID(Integer.parseInt(cur.getString(1)));
				news.SetCatID(Integer.parseInt(cur.getString(2)));
				news.setNtitle(cur.getString(3));
				news.setNintroText(cur.getString(4));
				news.setFullText(cur.getString(5));
				news.setNArtical(cur.getString(6));
				news.setImagePaht(cur.getString(7));
				news.setAuthor(cur.getString(8));
				news.SetCatIDs(Integer.parseInt(cur.getString(10)));
				news.setNewsCatName(cur.getString(11));

				newNews.add(news);

			} while (cur.moveToNext());
			cur.close();
		}

		db.close();

		return newNews;
	}

	public List<List_News> getNewID(List_News getNews) {
		List<List_News> newNews = new ArrayList<List_News>();

		SQLiteDatabase db = this.getWritableDatabase();

		List_News CatID = getNews;

		int gCatID = CatID.getCatid();

		Cursor cur = db.query(NewsTable, new String[] { colNewsid, colNewsCat,
				colTitle, colIntroText, colFullText, colArticalDate,
				colImagePath, colNewsAuthor }, colNewsCat + "=?",
				new String[] { Integer.toString(gCatID) }, null, null,
				colNewsid + " desc limit 1 ");

		if (cur.moveToFirst()) {
			do {
				List_News news = new List_News();
				news.SetID(Integer.parseInt(cur.getString(0)));
				news.SetCatID(Integer.parseInt(cur.getString(1)));
				news.setNtitle(cur.getString(2));
				news.setNintroText(cur.getString(3));
				news.setFullText(cur.getString(4));
				news.setNArtical(cur.getString(5));
				news.setImagePaht(cur.getString(6));
				news.setAuthor(cur.getString(7));

				newNews.add(news);
			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		return newNews;
	}

	public void DeleteNewsByCatID(int CatNewsid) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(NewsTable, colNewsCatid + "=?",
				new String[] { String.valueOf(CatNewsid) });
		db.close();
	}

	public void DeleteNewsFT() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(NewsTable, null, null);
		db.close();
	}

	/* ================SurveyForm==================== */

	public void AddSurveyForm(List_UserDetails addsubmit) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues cv = new ContentValues();
		cv.put(colFormSubmit, addsubmit.getFormSubmit());
		cv.put(colnotification, addsubmit.getnotification());
		cv.put(colsound, addsubmit.getsound());
		cv.put(colvibrate, addsubmit.getvibrate());
		cv.put(colpopup, addsubmit.getpopup());
		cv.put(colregid, addsubmit.getregid());

		db.insert(SurveyFormTable, null, cv);
		db.close();
	}

	public List<List_UserDetails> getSurveyFormSubmit() {
		List<List_UserDetails> SurveySubmit = new ArrayList<List_UserDetails>();

		String selectQuery = "SELECT * FROM " + SurveyFormTable;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery(selectQuery, null);

		if (cur.moveToFirst()) {
			do {
				List_UserDetails newsSurvey = new List_UserDetails();
				newsSurvey.SetID(Integer.parseInt(cur.getString(0)));
				newsSurvey.setFormSubmit(Integer.parseInt(cur.getString(1)));
				newsSurvey.setnotification(Integer.parseInt(cur.getString(2)));
				newsSurvey.setsound(Integer.parseInt(cur.getString(3)));
				newsSurvey.setvibrate(Integer.parseInt(cur.getString(4)));
				newsSurvey.setpopup(Integer.parseInt(cur.getString(5)));
				newsSurvey.setregid(cur.getString(6));

				SurveySubmit.add(newsSurvey);
			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		return SurveySubmit;
	}

	public int getnotification() {
		int number = 2;

		String selectQuery = "SELECT * FROM " + SurveyFormTable;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery(selectQuery, null);

		if (cur.moveToFirst()) {
			do {
				number = Integer.parseInt(cur.getString(2));

			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		return number;
	}

	public void updatenotification(int notification) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();

		cv.put(colnotification, notification);

		db.update(SurveyFormTable, cv, colFormid + "=?",
				new String[] { String.valueOf(1) });
		db.close();
	}

	public int getsound() {
		int number = 2;

		String selectQuery = "SELECT * FROM " + SurveyFormTable;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery(selectQuery, null);

		if (cur.moveToFirst()) {
			do {
				number = Integer.parseInt(cur.getString(3));

			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		return number;
	}

	public void updatesound(int sound) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();

		cv.put(colsound, sound);

		db.update(SurveyFormTable, cv, colFormid + "=?",
				new String[] { String.valueOf(1) });
		db.close();
	}

	public int getvibration() {
		int number = 2;

		String selectQuery = "SELECT * FROM " + SurveyFormTable;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery(selectQuery, null);

		if (cur.moveToFirst()) {
			do {
				number = Integer.parseInt(cur.getString(4));

			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		return number;
	}

	public void updatevibration(int vibration) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();

		cv.put(colvibrate, vibration);

		db.update(SurveyFormTable, cv, colFormid + "=?",
				new String[] { String.valueOf(1) });
		db.close();
	}

	public int getpopup() {
		int number = 2;

		String selectQuery = "SELECT * FROM " + SurveyFormTable;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery(selectQuery, null);

		if (cur.moveToFirst()) {
			do {
				number = Integer.parseInt(cur.getString(5));

			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		return number;
	}

	public void updatepopup(int popup) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();

		cv.put(colpopup, popup);

		db.update(SurveyFormTable, cv, colFormid + "=?",
				new String[] { String.valueOf(1) });
		db.close();
	}

	public String getregid() {
		String text = "";

		String selectQuery = "SELECT * FROM " + SurveyFormTable;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery(selectQuery, null);

		if (cur.moveToFirst()) {
			do {
				text = cur.getString(6);

			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		return text;
	}

	public void updateregid(String regid) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();

		cv.put(colregid, regid);

		db.update(SurveyFormTable, cv, colFormid + "=?",
				new String[] { String.valueOf(1) });
		db.close();
	}

	/* =============About Us=============== */
	public void AddAboutUs(List_AboutUsImage addAboutUsImage) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues cv = new ContentValues();

		cv.put(colAboutid, addAboutUsImage.getAboutUsID());
		cv.put(colAboutImagePath, addAboutUsImage.getAboutUsImgPath());
		cv.put(colAboutDate, addAboutUsImage.getAboutUsImgUpdateDate());
		cv.put(colAboutLink, addAboutUsImage.getAboutUsLink());

		db.insert(AboutUsTable, null, cv);
		db.close();
	}

	public void updateAboutUs(List_AboutUsImage AboutUsImageID) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();

		cv.put(colAboutid, AboutUsImageID.getAboutUsID());
		cv.put(colAboutImagePath, AboutUsImageID.getAboutUsImgPath());
		cv.put(colAboutDate, AboutUsImageID.getAboutUsImgUpdateDate());
		cv.put(colAboutLink, AboutUsImageID.getAboutUsLink());

		db.update(AboutUsTable, cv, colAboutid + "=?",
				new String[] { String.valueOf(AboutUsImageID.getAboutUsID()) });
		db.close();
	}

	public List<List_AboutUsImage> getAboutUs() {
		List<List_AboutUsImage> AboutUs = new ArrayList<List_AboutUsImage>();

		String selectQuery = "SELECT * FROM " + AboutUsTable;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery(selectQuery, null);

		if (cur.moveToFirst()) {
			do {
				List_AboutUsImage newsImg = new List_AboutUsImage();
				newsImg.setAboutUsImgID(Integer.parseInt(cur.getString(0)));
				newsImg.setAboutUsImgPath(cur.getString(1));
				newsImg.setAboutUsImgUpdateDate(cur.getString(2));
				newsImg.setAboutUsLink(cur.getString(3));

				AboutUs.add(newsImg);

			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		return AboutUs;
	}

	/* =============Font Setting=============== */
	public void AddFontSize(int fontSize) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues cv = new ContentValues();
		cv.put(colFontsize, fontSize);

		db.insert(FontSettingTable, null, cv);
		db.close();
	}

	public void updateFontSize(int FontSize) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();

		cv.put(colFontsize, FontSize);

		db.update(FontSettingTable, cv, colFontid + "=?",
				new String[] { String.valueOf(1) });

		db.close();
	}

	public int getFontSize() {
		int FontSize = 0;

		String selectQuery = "SELECT * FROM " + FontSettingTable;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery(selectQuery, null);

		if (cur.moveToFirst()) {
			do {
				FontSize = Integer.parseInt(cur.getString(1));

			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		return FontSize;
	}
	
	public void DeleteFont() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(FontSettingTable, null, null);
		db.close();
	}
}
