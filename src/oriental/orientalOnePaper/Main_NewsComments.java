package oriental.orientalOnePaper;

import android.app.ListActivity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class Main_NewsComments extends ListActivity {

	private Button btnpost;
	private ImageButton btnnews, btnexitpush;
	private EditText comment, commentposter;
	private int newsid, activitynumber, previousactivitynumber;
	private Bundle bundle;
	private String commenttext, commentpostertext;
	private Database_WebService webservice;
	private Config_ConstantVariable constant;
	private ListView lv;
	private String[] commentlist, commentposterlist, commentdate;
	private CustomAdapter_Comments adapter;
	private LinearLayout menu;
	private TextView title, title2;
	private ImageView home;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_newscomments);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		bundle = this.getIntent().getExtras();
		newsid = bundle.getInt("newsid");
		activitynumber = bundle.getInt("activitynumber");
		previousactivitynumber = bundle.getInt("previousactivitynumber");
		if (activitynumber == 5) {
			bundle.getString("newsid");
		} else if (activitynumber == 2) {
		}
		commentlist = bundle.getStringArray("comments");
		commentposterlist = bundle.getStringArray("commentsposter");
		commentdate = bundle.getStringArray("commentsdate");

		webservice = new Database_WebService(this);
		constant = new Config_ConstantVariable(this);

		btnnews = (ImageButton) findViewById(R.id.btn_news);
		btnnews.setVisibility(View.GONE);

		lv = (ListView) findViewById(android.R.id.list);

		if (previousactivitynumber == 1 || previousactivitynumber == 2) {
			bundle.putInt("activitynumber", 4);
			constant.OthersButton(bundle);
		}
		if (activitynumber == 5)
			Pushpage();
		constant.LatestNewsButton();
		constant.AllCategoryButton(-1, false);
		insertcomments();
		filldata();
	}

	@Override
	public void onDestroy() {
		lv = (ListView) findViewById(android.R.id.list);
		lv.setAdapter(null);
		super.onDestroy();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {

				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(Main_NewsComments.this,
				Main_ParticularNewsDetail.class);
		intent.putExtras(bundle);
		startActivity(intent);
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater menulayout = getMenuInflater();
		menulayout.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.setting) {
			Intent intent = new Intent(Main_NewsComments.this,
					Menu_Setting.class);
			startActivity(intent);
			return true;
		}
		return false;
	}

	private void Pushpage() {
		home = (ImageView) findViewById(R.id.image_home);
		home.setVisibility(View.GONE);
		menu = (LinearLayout) findViewById(R.id.layout_menu);
		menu.setVisibility(View.GONE);
		title = (TextView) findViewById(R.id.text_title);
		title.setVisibility(View.GONE);
		title2 = (TextView) findViewById(R.id.text_titlepush);
		title2.setVisibility(View.VISIBLE);
		btnexitpush = (ImageButton) findViewById(R.id.btn_exitpush);
		btnexitpush.setVisibility(View.VISIBLE);
		btnexitpush.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				Intent intent = new Intent(Main_NewsComments.this,
						Main_ParticularNewsDetail.class);
				intent.putExtras(bundle);
				startActivity(intent);
				finish();
			}
		});
	}

	private void filldata() {
		adapter = new CustomAdapter_Comments(this, commentlist,
				commentposterlist, commentdate);
		if (constant.isOnline()) {
			lv.addHeaderView(constant.AdMob());
			lv.addFooterView(constant.AdMob());
		}
		lv.setAdapter(adapter);
	}

	private void insertcomments() {
		comment = (EditText) findViewById(R.id.inserttext_comment);
		commentposter = (EditText) findViewById(R.id.inserttext_commentposter);

		btnpost = (Button) findViewById(R.id.btn_post);
		btnpost.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!(comment.getText().toString().trim().equals(""))) {
					commenttext = comment.getText().toString();
					if ((commentposter.getText().toString().trim().equals(""))) {
						commentpostertext = Config_ConstantVariable.commentposter;
					} else {
						commentpostertext = commentposter.getText().toString();
					}
					if (constant.isOnline()) {
						webservice.InsertComment(newsid, commentpostertext,
								commenttext, 0);
						constant.StartAnimation(1);
						new Handler().postDelayed(new Runnable() {
							public void run() {
								if (webservice.success) {
									constant.ShowMessage(Config_ConstantVariable.warnmsg_commentapprove);
									Intent intent = new Intent(
											Main_NewsComments.this,
											Main_ParticularNewsDetail.class);
									intent.putExtras(bundle);
									startActivity(intent);
									finish();
								} else {
									constant.ShowMessage(Config_ConstantVariable.warnmsg_serverblockpostcomment);
									constant.StopAnimation();
								}
							}
						}, 1 * 1000);
					} else {
						constant.ShowMessage(Config_ConstantVariable.warnmsg_blockpostcomment);
					}
				} else {
					constant.ShowMessage(Config_ConstantVariable.warnmsg_nocomment);
				}
			}
		});
	}
}
