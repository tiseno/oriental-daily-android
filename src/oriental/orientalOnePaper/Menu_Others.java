package oriental.orientalOnePaper;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class Menu_Others extends Activity {
	private Button btnsuggestion, btnaboutus, btnordernp, btnorderads,
			btnordersms;
	private ImageButton btnothers;
	private Config_ConstantVariable constant;
	private ScrollView sv;
	private LinearLayout linear;
	private Bundle bundle;
	private int activityno;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_others);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		bundle = this.getIntent().getExtras();
		activityno = bundle.getInt("activitynumber");
		constant = new Config_ConstantVariable(this);
		constant.AllCategoryButton(-1, false);
		constant.ParticularCategoryButton(0);
		constant.LatestNewsButton();
		btnothers = (ImageButton) findViewById(R.id.btn_others);
		btnothers.setBackgroundResource(R.color.button_changeother_active);
		Menu();
		AnimateSidebar();
	}

	@Override
	public void onBackPressed() {
		if (activityno == 1) {
			Intent intent = new Intent(Menu_Others.this,
					Main_AllLatestNews.class);
			intent.putExtras(bundle);
			startActivity(intent);
			finish();
		} else if (activityno == 2) {
			Intent intent = new Intent(Menu_Others.this,
					Main_ParticularCategoryAllNews.class);
			intent.putExtras(bundle);
			startActivity(intent);
			finish();
		} else if (activityno == 3) {
			Intent intent = new Intent(Menu_Others.this,
					Main_ParticularNewsDetail.class);
			intent.putExtras(bundle);
			startActivity(intent);
			finish();
		} else if (activityno == 4) {
			Intent intent = new Intent(Menu_Others.this,
					Main_NewsComments.class);
			intent.putExtras(bundle);
			startActivity(intent);
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater menulayout = getMenuInflater();
		menulayout.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.setting) {
			Intent intent = new Intent(Menu_Others.this, Menu_Setting.class);
			startActivity(intent);
			return true;
		}
		return false;
	}

	private void AnimateSidebar() {
		sv = (ScrollView) findViewById(R.id.scrollView1);
		sv.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (constant.menuOut) {
					constant.Animation();
				}
				return false;
			}
		});

		linear = (LinearLayout) findViewById(R.id.layout_content);
		linear.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (constant.menuOut) {
					constant.Animation();
				}
				return false;
			}
		});
	}

	public void Menu() {
		btnordersms = (Button) findViewById(R.id.btn_ordersms);
		btnordersms.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				constant.StartAnimation(1);
				new Handler().postDelayed(new Runnable() {
					public void run() {
						Intent intent = new Intent(Menu_Others.this,
								Menu_OthersOrderSMS.class);
						intent.putExtras(bundle);
						startActivity(intent);
						finish();
					}
				}, 1 * 1000);
			}
		});

		btnordernp = (Button) findViewById(R.id.btn_ordernp);
		btnordernp.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				constant.StartAnimation(1);
				new Handler().postDelayed(new Runnable() {
					public void run() {
						Intent intent = new Intent(Menu_Others.this,
								Menu_OthersOrderNewspaper.class);
						intent.putExtras(bundle);
						startActivity(intent);
						finish();
					}
				}, 1 * 1000);
			}
		});

		btnorderads = (Button) findViewById(R.id.btn_orderads);
		btnorderads.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				constant.StartAnimation(1);
				new Handler().postDelayed(new Runnable() {
					public void run() {
						Intent intent = new Intent(Menu_Others.this,
								Menu_OthersOrderAdvertisement.class);
						intent.putExtras(bundle);
						startActivity(intent);
						finish();
					}
				}, 1 * 1000);
			}
		});

		btnaboutus = (Button) findViewById(R.id.btn_aboutus);
		btnaboutus.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				constant.StartAnimation(1);
				new Handler().postDelayed(new Runnable() {
					public void run() {
						Intent intent = new Intent(Menu_Others.this,
								Menu_OthersAboutUsWebsite.class);
						bundle.putString("type", "aboutus");
						intent.putExtras(bundle);
						startActivity(intent);
						finish();
					}
				}, 1 * 1000);
			}
		});

		btnsuggestion = (Button) findViewById(R.id.btn_suggestion);
		btnsuggestion.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				constant.StartAnimation(1);
				new Handler().postDelayed(new Runnable() {
					public void run() {
						Intent intent = new Intent(Menu_Others.this,
								Menu_OthersSuggestion.class);
						intent.putExtras(bundle);
						startActivity(intent);
						finish();
					}
				}, 1 * 1000);
			}
		});
	}
}
