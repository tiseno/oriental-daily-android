package oriental.orientalOnePaper;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class Main_ParticularNewsDetail extends Activity {
	private String get_CountComment_SOAP_ACTION = "OrientalServices1#responseGetCountComment";
	private String get_CountComment_METHOD_NAME = "GetCountComment";
	private String get_Comment_SOAP_ACTION = "OrientalServices1#Get_CommentByNewsID";
	private String get_Comment_METHOD_NAME = "Get_CommentByNewsID";
	private ImageButton btnfont, btnshare, btncomments, btnback, btnnews,
			btnexitpush, btnothers;
	private boolean on = true, isshow, outdated = false;
	private TextView newsdetail, newstitle, newsdate, commentsnumber, newscat,
			commentsnumbertext, newsauthor, newsauthortext, newsreporter,
			newsreportertext, title, title2, click;
	private Bundle bundle;
	private SeekBar sbfont;
	private Database_WebService webservice;
	private String imagepath, getPropertyID, pagetitletext, payload;
	private ImageLoader_Loader imageloader;
	private Config_ConstantVariable constant;
	private ImageView imageview, home;
	private int max = 30, min = 10, newsid, catnewsid, position, index,
			activitynumber, previousactivitynumber, fontsize;
	private LinearLayout linear;
	private List<List_NewsComment> sallCommentList;
	private String[] commentlist, commentposterlist, commentdate;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_particularnewsdetail);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		constant = new Config_ConstantVariable(this);
		bundle = this.getIntent().getExtras();
		activitynumber = bundle.getInt("activitynumber");
		previousactivitynumber = bundle.getInt("previousactivitynumber");

		Button();

		if (activitynumber == 5) {
			payload = bundle.getString("newsid");
			newsid = Integer.parseInt(payload);
			home = (ImageView) findViewById(R.id.image_home);
			home.setVisibility(View.GONE);
			title = (TextView) findViewById(R.id.text_title);
			title.setVisibility(View.GONE);
			title2 = (TextView) findViewById(R.id.text_titlepush);
			title2.setVisibility(View.VISIBLE);

			webservice = new Database_WebService(this, this, newsid);
			webservice.LoadAllNews();

			for (int i = 0; i < webservice.news.size(); i++) {
				if (newsid == webservice.news.get(i).getID()) {
					outdated = true;
					break;
				} else
					continue;
			}
			if (!outdated) {
				if (constant.isOnline()) {
					constant.ShowMessage(Config_ConstantVariable.warnmsg_getting);
					constant.StartAnimation(1000);
					webservice.UpdateAllCatNews(6);
				} else {
					constant.ShowMessage(Config_ConstantVariable.warnmsg_nowifi);
					Intent intent = new Intent(Main_ParticularNewsDetail.this,
							Main_AllLatestNews.class);
					bundle.putBoolean("isshow", false);
					bundle.putBoolean("needdownload", false);
					intent.putExtras(bundle);
					startActivity(intent);
					finish();
				}
			} else {
				webservice.LoadSingleNews(newsid);
				sbfont = (SeekBar) findViewById(R.id.sb_font);
				commentsnumber = (TextView) findViewById(R.id.text_commentsnumber);
				fontsize = webservice.ReadFontSize();
				filldata();
				Button();
				FontControl();
			}
		} else {
			newsid = bundle.getInt("newsid");
			webservice = new Database_WebService(this, this, newsid);
			webservice.LoadSingleNews(newsid);
			sbfont = (SeekBar) findViewById(R.id.sb_font);
			commentsnumber = (TextView) findViewById(R.id.text_commentsnumber);

			fontsize = webservice.ReadFontSize();
			filldata();
			Button();
			FontControl();
		}
	}

	@Override
	public void onBackPressed() {
		if (!on) {
			sbfont.setVisibility(View.INVISIBLE);
			webservice.UpdateFontSize(fontsize);
			on = true;
		} else {
			if (previousactivitynumber == 1) {
				Intent intent = new Intent(Main_ParticularNewsDetail.this,
						Main_AllLatestNews.class);
				bundle.putBoolean("isshow", false);
				bundle.putBoolean("needdownload", false);
				bundle.putBoolean("isfirsttime", false);
				intent.putExtras(bundle);
				startActivity(intent);
				finish();
			} else if (previousactivitynumber == 2) {
				Intent intent = new Intent(Main_ParticularNewsDetail.this,
						Main_ParticularCategoryAllNews.class);
				intent.putExtras(bundle);
				startActivity(intent);
				finish();
			} else if (previousactivitynumber == 6 || activitynumber == 5)
				WarningMessage();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater menulayout = getMenuInflater();
		menulayout.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.setting) {
			Intent intent = new Intent(Main_ParticularNewsDetail.this,
					Menu_Setting.class);
			startActivity(intent);
			return true;
		}
		return false;
	}

	private void WarningMessage() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(Config_ConstantVariable.alertmsgpush)
				.setCancelable(false)
				.setPositiveButton(Config_ConstantVariable.alertbtnyes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								finish();
							}
						})
				.setNegativeButton(Config_ConstantVariable.alertbtnno,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void PopoutNews() {
		imagepath = imagepath.replace(imagepath.charAt(imagepath.length() - 5),
				'L');
		ImageView temp = new ImageView(this);
		imageloader.DisplayImage(imagepath, temp);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setView(temp)
				.setCancelable(false)
				.setPositiveButton(Config_ConstantVariable.alertbtnback,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void filldata() {
		try {
			newscat = (TextView) findViewById(R.id.text_pagetitle);
			newscat.setText(webservice.news.get(0).getNewCatName());

			newstitle = (TextView) findViewById(R.id.text_newstitle);
			newstitle.setText(webservice.news.get(0).getNtitle());

			newsdetail = (TextView) findViewById(R.id.text_newsdetail);
			newsdetail.setTextSize(fontsize);

			newsdate = (TextView) findViewById(R.id.text_newsdate);
			newsdate.setText(webservice.news.get(0).getNArticalD());

			if (!webservice.news.get(0).getAuthor().equals("empty")) {
				catnewsid = webservice.news.get(0).getCatID();
				if (catnewsid == 6 || catnewsid == 10 || catnewsid == 11) {
					newsauthortext = (TextView) findViewById(R.id.fixtext_newsauthor);
					newsauthortext.setVisibility(View.VISIBLE);

					newsauthor = (TextView) findViewById(R.id.text_newsauthor);
					newsauthor.setVisibility(View.VISIBLE);
					newsauthor.setText(webservice.news.get(0).getAuthor());
					
					Log.i("author", ""+webservice.news.get(0).getAuthor());
				} else if (catnewsid == 7) {
					newsreportertext = (TextView) findViewById(R.id.fixtext_newsreporter);
					newsreportertext.setVisibility(View.VISIBLE);

					newsreporter = (TextView) findViewById(R.id.text_newsreporter);
					newsreporter.setVisibility(View.VISIBLE);
					newsreporter.setText(webservice.news.get(0).getAuthor());
				}
			}

			linear = (LinearLayout) findViewById(R.id.layout_image);
			imageview = (ImageView) findViewById(R.id.image_title);
			click = (TextView) findViewById(R.id.fixtext_image);

			imagepath = webservice.news.get(0).getImagePath();
			if (!imagepath.toString().equals("no picture")) {
				imagepath = imagepath.replace(
						imagepath.charAt(imagepath.length() - 5), 'M');
				new Handler().postDelayed(new Runnable() {
					public void run() {
						@SuppressWarnings("deprecation")
						RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
								ViewGroup.LayoutParams.FILL_PARENT,
								ViewGroup.LayoutParams.WRAP_CONTENT);
						lp.addRule(RelativeLayout.BELOW, R.id.layout_image);
						lp.setMargins(8, 0, 8, 0);
						LinearLayout a = (LinearLayout) findViewById(R.id.layoutdetailndate);
						a.setLayoutParams(lp);
						newsdetail.setText(webservice.news.get(0)
								.getNintrotext()
								+ webservice.news.get(0).getFullText());
					}
				}, 1 * 1000);
				click.setPadding(0, linear.getHeight() / 2, 0, 0);
				click.setVisibility(View.VISIBLE);
				imageloader = new ImageLoader_Loader(this);
				imageloader.DisplayImage(imagepath, imageview);
				imageview.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						if (constant.isOnline())
							PopoutNews();
					}
				});
			} else {
				click.setVisibility(View.GONE);
				linear.setVisibility(View.GONE);
				newsdetail.setText(webservice.news.get(0).getNintrotext()
						+ webservice.news.get(0).getFullText());
			}

			if (constant.isOnline()) {
				GetCountCommentNews(newsid);
				new Handler().postDelayed(new Runnable() {
					public void run() {
						commentsnumber.setVisibility(View.VISIBLE);
						commentsnumber.setText(getPropertyID);
					}
				}, 5 * 1000);
			} else {
				commentsnumber.setVisibility(View.VISIBLE);
			}

		} catch (Exception ex) {
		}
		if (constant.isOnline()) {
			constant.AdsMob();
			constant.AdsMob1();
		}
	}

	private void Button() {
		btnnews = (ImageButton) findViewById(R.id.btn_news);
		btnnews.setVisibility(View.GONE);

		btnfont = (ImageButton) findViewById(R.id.btn_font);
		btnfont.setLayoutParams(constant.margin(btnfont, 4));
		btnfont.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				if (on) {
					sbfont.setMax(max);
					sbfont.setProgress(fontsize - min);
					sbfont.setVisibility(View.VISIBLE);
					on = false;
				} else {
					sbfont.setVisibility(View.INVISIBLE);
					webservice.UpdateFontSize(fontsize);
					on = true;
				}
			}
		});

		btnshare = (ImageButton) findViewById(R.id.btn_share);
		btnshare.setLayoutParams(constant.margin(btnshare, 4));
		btnshare.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				if (constant.isOnline()) {
					Intent sharingIntent = new Intent(
							android.content.Intent.ACTION_SEND);
					sharingIntent.setType("text/plain");

					sharingIntent.putExtra(Intent.EXTRA_TEXT,
							Config_ConstantVariable.sharelink + newsid);
					startActivity(Intent.createChooser(sharingIntent,
							"Share via"));
				} else {
					constant.ShowMessage(Config_ConstantVariable.warnmsg_blocksharenews);
				}
			}
		});

		btncomments = (ImageButton) findViewById(R.id.btn_comments);
		btncomments.setLayoutParams(constant.margin(btncomments, 4));
		btncomments.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				AccessCommentPage();
			}
		});

		commentsnumbertext = (TextView) findViewById(R.id.fixtext_commentsnumber);
		commentsnumbertext.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				AccessCommentPage();
			}
		});

		if (activitynumber != 6) {
			bundle.putInt("activitynumber", 3);
			constant.OthersButton(bundle);
			constant.LatestNewsButton();
			btnback = (ImageButton) findViewById(R.id.btn_back);
			btnback.setVisibility(View.VISIBLE);
			btnback.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					if (!on) {
						sbfont.setVisibility(View.INVISIBLE);
						webservice.UpdateFontSize(fontsize);
						on = true;
					} else {
						if (previousactivitynumber == 1) {
							Intent intent = new Intent(
									Main_ParticularNewsDetail.this,
									Main_AllLatestNews.class);
							bundle.putBoolean("isshow", false);
							bundle.putBoolean("needdownload", false);
							bundle.putBoolean("isfirsttime", false);
							intent.putExtras(bundle);
							startActivity(intent);
							finish();
						} else if (previousactivitynumber == 2) {
							catnewsid = bundle.getInt("catnewsid");
							pagetitletext = bundle.getString("pagetitle");
							position = bundle.getInt("position");
							isshow = bundle.getBoolean("isshow");
							index = bundle.getInt("scrollposition");
							Intent intent = new Intent(
									Main_ParticularNewsDetail.this,
									Main_ParticularCategoryAllNews.class);
							bundle.putInt("catnewsid", catnewsid);
							bundle.putString("pagetitle", pagetitletext);
							bundle.putInt("position", position);
							bundle.putBoolean("isshow", isshow);
							bundle.putInt("scrollposition", index);
							intent.putExtras(bundle);
							startActivity(intent);
							finish();
						}
					}
				}
			});
		}

		if (previousactivitynumber == 6 || activitynumber == 5) {
			btnothers = (ImageButton) findViewById(R.id.btn_others);
			btnothers.setVisibility(View.GONE);
			btnfont.setLayoutParams(constant.margin(btnfont, 3));
			btnshare.setLayoutParams(constant.margin(btnshare, 3));
			btncomments.setLayoutParams(constant.margin(btncomments, 3));
			title = (TextView) findViewById(R.id.text_title);
			title.setVisibility(View.GONE);
			title2 = (TextView) findViewById(R.id.text_titlepush);
			title2.setVisibility(View.VISIBLE);
			home = (ImageView) findViewById(R.id.image_home);
			home.setVisibility(View.GONE);
			btnback = (ImageButton) findViewById(R.id.btn_back);
			btnback.setVisibility(View.GONE);
			btnexitpush = (ImageButton) findViewById(R.id.btn_exitpush);
			btnexitpush.setVisibility(View.VISIBLE);
			btnexitpush.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					WarningMessage();
				}
			});
		}
	}

	private void AccessCommentPage() {
		if (constant.isOnline()) {
			btnshare.setEnabled(false);
			btnfont.setEnabled(false);
			btncomments.setEnabled(false);
			commentsnumbertext.setEnabled(false);
			constant.DisableOthersButton();
			constant.StartAnimation(1000);
			GetCommentNews(newsid);
		} else {
			constant.ShowMessage(Config_ConstantVariable.warnmsg_serverblockaccesscomment);
		}
	}

	private void FontControl() {
		sbfont.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				fontsize = arg1 + min;
				newsdetail.setTextSize(fontsize);
				webservice.UpdateFontSize(arg1 + min);
			}

			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			public void onStopTrackingTouch(SeekBar seekBar) {

			}

		});
	}

	private void GetCountCommentNews(final int gCommentNewsID) {
		Thread networkThread = new Thread() {
			@Override
			public void run() {
				try {
					SoapObject request = new SoapObject(
							Database_WebService.NAMESPACE,
							get_CountComment_METHOD_NAME);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					HttpTransportSE androidHttpTransportSE = new HttpTransportSE(
							Database_WebService.URL);

					request.addProperty("itemid", gCommentNewsID);
					envelope.setOutputSoapObject(request);
					androidHttpTransportSE.call(get_CountComment_SOAP_ACTION,
							envelope);
					SoapObject result = (SoapObject) envelope.bodyIn;
					RetrieveCountCommentFromSoap(result);
				} catch (Exception ex) {
					commentsnumber.setVisibility(View.VISIBLE);
				}
			}
		};
		networkThread.start();
	}

	private void RetrieveCountCommentFromSoap(SoapObject soap) {
		Vector<Object> property2 = Database_WebService.extracted(soap);
		SoapObject getPropertyD = (SoapObject) property2.get(0);
		getPropertyID = getPropertyD.getProperty("count").toString();
	}

	private void GetCommentNews(final int gCommentNewsID) {
		Thread networkThread = new Thread() {
			@Override
			public void run() {
				try {
					SoapObject request = new SoapObject(
							Database_WebService.NAMESPACE,
							get_Comment_METHOD_NAME);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					HttpTransportSE androidHttpTransportSE = new HttpTransportSE(
							Database_WebService.URL);

					request.addProperty("itemid", gCommentNewsID);
					envelope.setOutputSoapObject(request);
					androidHttpTransportSE.call(get_Comment_SOAP_ACTION,
							envelope);

					SoapObject result = (SoapObject) envelope.bodyIn;
					RetrieveFromSoap(result);
				} catch (Exception ex) {
					Intent intent = new Intent(Main_ParticularNewsDetail.this,
							Main_ParticularNewsDetail.class);
					bundle.putInt("newsid", newsid);
					bundle.putInt("activitynumber", activitynumber);
					intent.putExtras(bundle);
					startActivity(intent);
					finish();
				}
			}

		};
		networkThread.start();
	}

	private List<List_NewsComment> RetrieveFromSoap(SoapObject soap) {
		sallCommentList = new ArrayList<List_NewsComment>();
		Vector<Object> property2 = Database_WebService.extracted(soap);

		for (int i = 0; i < property2.size(); i++) {

			SoapObject getPropertyD = (SoapObject) property2.get(i);

			List_NewsComment addcomment = new List_NewsComment();
			addcomment.setcommentID(Integer.parseInt(getPropertyD.getProperty(
					"id").toString()));
			addcomment.setcommentDate(getPropertyD.getProperty("date")
					.toString());
			addcomment.setItemID(Integer.parseInt(getPropertyD.getProperty(
					"item_id").toString()));
			addcomment.setUserName(getPropertyD.getProperty("name").toString());
			addcomment.setCommentContent(getPropertyD.getProperty("comment")
					.toString());
			addcomment.setAproveStauts(Integer.parseInt(getPropertyD
					.getProperty("approval_status").toString()));

			sallCommentList.add(addcomment);
		}

		if (sallCommentList.size() != 0) {
			commentlist = new String[sallCommentList.size()];
			commentposterlist = new String[sallCommentList.size()];
			commentdate = new String[sallCommentList.size()];
			for (int i = 0; i < sallCommentList.size(); i++) {
				commentlist[i] = sallCommentList.get(i).getCommentContent();
				commentposterlist[i] = sallCommentList.get(i).getUserName();
				commentdate[i] = sallCommentList.get(i).getCommentDate();
			}
		} else {
			commentlist = new String[1];
			commentposterlist = new String[1];
			commentdate = new String[1];
			commentlist[0] = Config_ConstantVariable.commentgm;
			commentposterlist[0] = Config_ConstantVariable.commentgmname;
			commentdate[0] = Config_ConstantVariable.date;
		}

		Intent intent = new Intent(Main_ParticularNewsDetail.this,
				Main_NewsComments.class);
		bundle.putInt("newsid", newsid);
		bundle.putInt("activitynumber", activitynumber);
		bundle.putStringArray("comments", commentlist);
		bundle.putStringArray("commentsposter", commentposterlist);
		bundle.putStringArray("commentsdate", commentdate);
		if (activitynumber == 2) {
			catnewsid = bundle.getInt("catnewsid");
			pagetitletext = bundle.getString("pagetitle");
			position = bundle.getInt("position");
			isshow = bundle.getBoolean("isshow");
			index = bundle.getInt("scrollposition");
			bundle.putInt("catnewsid", catnewsid);
			bundle.putString("pagetitle", pagetitletext);
			bundle.putInt("position", position);
			bundle.putBoolean("isshow", isshow);
			bundle.putInt("scrollposition", index);
		} else if (activitynumber == 5) {
			bundle.putString("newsid", payload);
		}
		intent.putExtras(bundle);
		startActivity(intent);
		finish();

		return sallCommentList;
	}
}
