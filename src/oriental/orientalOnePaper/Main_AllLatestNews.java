package oriental.orientalOnePaper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class Main_AllLatestNews extends ListActivity {
	private String title[], date[], imagepath[], category[], cat[], deviceId,
			RegId;
	private ImageButton btndownload;
	private ImageView home;
	private Database_WebService webservice;
	private Bundle bundle;
	private int[] newsid, catnewsid;
	private ListView lv;
	private CustomAdapter_LatestNews adapter;
	private Config_ConstantVariable constant;
	private LinearLayout linear;
	private boolean isshow = false, needdownload = false, ispublished,
			isfirsttime;
	private AutoDownload autodownload;
	private int temp, position;

	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private Calendar cal = Calendar.getInstance();
	private String datenow;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_alllatestnews);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		try {
			cal.add(Calendar.DATE, -1);
			datenow = dateFormat.format(cal.getTime()).toString();
			bundle = this.getIntent().getExtras();
			isshow = bundle.getBoolean("isshow");
			position = bundle.getInt("position");
			needdownload = bundle.getBoolean("needdownload");

			webservice = new Database_WebService(this, this);
			constant = new Config_ConstantVariable(this);

			if (isshow) {
				constant.ShowMessage(Config_ConstantVariable.warnmsg_wifiserverdownrefresh);
				Intent intent = new Intent(this, Main_AllLatestNews.class);
				bundle.putBoolean("isshow", false);
				intent.putExtras(bundle);
				startActivity(intent);
				finish();
			}

			home = (ImageView) findViewById(R.id.image_home);
			home.setVisibility(View.GONE);

			webservice.LoadtodayNews();
			constant.ParticularCategoryButton(0);
			constant.AllCategoryButton(position, true);
			bundle.putBoolean("isshow", false);
			bundle.putBoolean("needdownload", false);
			bundle.putBoolean("isfirsttime", false);
			bundle.putInt("activitynumber", 1);
			constant.OthersButton(bundle);

			lv = (ListView) findViewById(android.R.id.list);
			DownloadLatestNewsButton();
			AnimateSidebar();
			filldata();

			if (needdownload) {
				CheckRegistrationId();
				constant.StartAnimation(1000);
				if (webservice.news.size() > 0) {
					if (constant.isOnline()) {
						for (int i = 0; i < webservice.news.size(); i++) {
							if (datenow.compareTo(webservice.news.get(i)
									.getNArticalD()) > 0) {
								isfirsttime = bundle.getBoolean("isfirsttime");
								if (!isfirsttime)
									constant.ShowMessage(Config_ConstantVariable.warnmsg_downloading);
								btndownload.setEnabled(false);
								lv.setEnabled(false);
								constant.DisableAllCategoryButton();
								constant.DisableParticularCategoryButton();
								constant.DisableOthersButton();
								autodownload = new AutoDownload();
								autodownload.execute();
								break;
							} else {
								if (i >= webservice.news.size() - 1) {
									constant.StopAnimation();
									constant.ShowMessage(Config_ConstantVariable.warnmsg_nodownloading);
								} else
									continue;
							}
						}
					} else {
						constant.StopAnimation();
						constant.ShowMessage(Config_ConstantVariable.warnmsg_nowifiautofailed);
					}
				} else {
					if (constant.isOnline()) {
						isfirsttime = bundle.getBoolean("isfirsttime");
						if (!isfirsttime)
							constant.ShowMessage(Config_ConstantVariable.warnmsg_downloading);
						btndownload.setEnabled(false);
						lv.setEnabled(false);
						constant.DisableAllCategoryButton();
						constant.DisableParticularCategoryButton();
						constant.DisableOthersButton();
						autodownload = new AutoDownload();
						autodownload.execute();
					} else {
						constant.StopAnimation();
						constant.ShowMessage(Config_ConstantVariable.warnmsg_nowifiautofailed);
					}
				}
			}
		} catch (Exception e) {
			webservice = new Database_WebService(this, this);
			constant = new Config_ConstantVariable(this);
			
			webservice.DeleteNews();
			cal.add(Calendar.DATE, -1);
			datenow = dateFormat.format(cal.getTime()).toString();
			bundle = this.getIntent().getExtras();
			isshow = bundle.getBoolean("isshow");
			position = bundle.getInt("position");
			needdownload = bundle.getBoolean("needdownload");

			if (isshow) {
				constant.ShowMessage(Config_ConstantVariable.warnmsg_wifiserverdownrefresh);
				Intent intent = new Intent(this, Main_AllLatestNews.class);
				bundle.putBoolean("isshow", false);
				intent.putExtras(bundle);
				startActivity(intent);
				finish();
			}

			home = (ImageView) findViewById(R.id.image_home);
			home.setVisibility(View.GONE);

			webservice.LoadtodayNews();
			constant.ParticularCategoryButton(0);
			constant.AllCategoryButton(position, true);
			bundle.putBoolean("isshow", false);
			bundle.putBoolean("needdownload", false);
			bundle.putBoolean("isfirsttime", false);
			bundle.putInt("activitynumber", 1);
			constant.OthersButton(bundle);

			lv = (ListView) findViewById(android.R.id.list);
			DownloadLatestNewsButton();
			AnimateSidebar();
			filldata();

			if (needdownload) {
				CheckRegistrationId();
				constant.StartAnimation(1000);
				if (webservice.news.size() > 0) {
					if (constant.isOnline()) {
						for (int i = 0; i < webservice.news.size(); i++) {
							if (datenow.compareTo(webservice.news.get(i)
									.getNArticalD()) > 0) {
								isfirsttime = bundle.getBoolean("isfirsttime");
								if (!isfirsttime)
									constant.ShowMessage(Config_ConstantVariable.warnmsg_downloading);
								btndownload.setEnabled(false);
								lv.setEnabled(false);
								constant.DisableAllCategoryButton();
								constant.DisableParticularCategoryButton();
								constant.DisableOthersButton();
								autodownload = new AutoDownload();
								autodownload.execute();
								break;
							} else {
								if (i >= webservice.news.size() - 1) {
									constant.StopAnimation();
									constant.ShowMessage(Config_ConstantVariable.warnmsg_nodownloading);
								} else
									continue;
							}
						}
					} else {
						constant.StopAnimation();
						constant.ShowMessage(Config_ConstantVariable.warnmsg_nowifiautofailed);
					}
				} else {
					if (constant.isOnline()) {
						isfirsttime = bundle.getBoolean("isfirsttime");
						if (!isfirsttime)
							constant.ShowMessage(Config_ConstantVariable.warnmsg_downloading);
						btndownload.setEnabled(false);
						lv.setEnabled(false);
						constant.DisableAllCategoryButton();
						constant.DisableParticularCategoryButton();
						constant.DisableOthersButton();
						autodownload = new AutoDownload();
						autodownload.execute();
					} else {
						constant.StopAnimation();
						constant.ShowMessage(Config_ConstantVariable.warnmsg_nowifiautofailed);
					}
				}
			}
		}
	}

	@Override
	public void onDestroy() {
		lv = (ListView) findViewById(android.R.id.list);
		lv.setAdapter(null);
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater menulayout = getMenuInflater();
		menulayout.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.setting) {
			Intent intent = new Intent(Main_AllLatestNews.this,
					Menu_Setting.class);
			startActivity(intent);
			return true;
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(Config_ConstantVariable.alertmsg)
				.setCancelable(false)
				.setPositiveButton(Config_ConstantVariable.alertbtnyes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								finish();
							}
						})
				.setNegativeButton(Config_ConstantVariable.alertbtnno,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void AnimateSidebar() {
		linear = (LinearLayout) findViewById(R.id.layout_content);
		linear.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (constant.menuOut) {
					constant.Animation();
				}
				return false;
			}
		});
	}

	private void DownloadLatestNewsButton() {
		btndownload = (ImageButton) findViewById(R.id.btn_download);
		btndownload.setVisibility(View.VISIBLE);
		btndownload.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				WarningMessage();
			}
		});
	}

	private void WarningMessage() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(Config_ConstantVariable.alertmsgdownload)
				.setCancelable(false)
				.setPositiveButton(Config_ConstantVariable.alertbtnyes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								if (constant.isOnline()) {
									constant.StartAnimation(1000);
									btndownload.setEnabled(false);
									lv.setEnabled(false);
									webservice.UpdateAllCatNews(1);
									constant.DisableAllCategoryButton();
									constant.DisableParticularCategoryButton();
									constant.DisableOthersButton();
								} else {
									constant.ShowMessage(Config_ConstantVariable.warnmsg_nowifi);
								}
							}
						})
				.setNegativeButton(Config_ConstantVariable.alertbtnno,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void filldata() {
		TextView titletext = (TextView) findViewById(R.id.text_pagetitle);
		titletext.setText(Config_ConstantVariable.latest);
		try {
			newsid = new int[webservice.news.size()];
			title = new String[webservice.news.size()];
			category = new String[webservice.news.size()];
			date = new String[webservice.news.size()];
			imagepath = new String[webservice.news.size()];
			catnewsid = new int[webservice.news.size()];
			cat = new String[webservice.news.size()];
			for (int i = 0; i < webservice.news.size(); i++) {
				newsid[i] = webservice.news.get(i).getID();
				title[i] = webservice.news.get(i).getNtitle();
				category[i] = webservice.news.get(i).getNewCatName();
				date[i] = webservice.news.get(i).getNArticalD();
				imagepath[i] = webservice.news.get(i).getImagePath();
				catnewsid[i] = webservice.news.get(i).getCatID();
				cat[i] = webservice.news.get(i).getNewCatName();
			}
			adapter = new CustomAdapter_LatestNews(this, title, category, date,
					imagepath);
			if (constant.isOnline()) {
				lv.addHeaderView(constant.AdMob());
				lv.addFooterView(constant.AdMob());
				ispublished = true;
			} else {
				ispublished = false;
			}
			lv.setAdapter(adapter);
			lv.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> arg0, View arg1,
						final int arg2, long arg3) {
					if (!constant.menuOut) {
						constant.StartAnimation(1);
						new Handler().postDelayed(new Runnable() {
							public void run() {
								if (ispublished) {
									temp = arg2 - 1;
								} else {
									temp = arg2;
								}
								if (catnewsid[temp] != 9) {
									Intent intent = new Intent(
											Main_AllLatestNews.this,
											Main_ParticularNewsDetail.class);
									bundle.putInt("newsid", newsid[temp]);
									bundle.putInt("previousactivitynumber", 1);
									bundle.putInt("activitynumber", 1);
									intent.putExtras(bundle);
									startActivity(intent);
									finish();
								} else {
									Intent intent = new Intent(
											Main_AllLatestNews.this,
											Menu_OthersAboutUsWebsite.class);
									bundle.putString("type", "special");
									bundle.putString("link",
											"http://www.orientaldaily.com.my/index.php?option=com_k2&view=item&id="
													+ newsid[temp]
													+ ":&Itemid=223");
									intent.putExtras(bundle);
									startActivity(intent);
									constant.StopAnimation();
								}
							}

						}, 1 * 1000);
					} else {
						constant.Animation();
					}
				}
			});
		} catch (Exception ex) {
		}
	}

	private void CheckRegistrationId() {
		deviceId = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
		RegId = C2DM_Registration.getRegistrationID(Main_AllLatestNews.this);
		if (!RegId.equals("")) {
			webservice.Updateregid(webservice.Readregid());
			C2DM_Registration.register(Main_AllLatestNews.this,
					Config_ConstantVariable.sender);
			new Handler().postDelayed(new Runnable() {
				public void run() {
					webservice.InsertToken(
							Config_ConstantVariable.commentposter, deviceId,
							RegId);
				}
			}, 2 * 1000);
		}
	}

	private class AutoDownload extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... params) {
			webservice.UpdateAllCatNews(1);
			return "finish";
		}
	}
}
