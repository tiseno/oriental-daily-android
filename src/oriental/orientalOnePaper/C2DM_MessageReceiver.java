package oriental.orientalOnePaper;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class C2DM_MessageReceiver extends BroadcastReceiver {
	private Database_WebService webservice;

	@Override
	public void onReceive(Context context, Intent intent) {
		webservice = new Database_WebService(context);
		String action = intent.getAction();
		if ("com.google.android.c2dm.intent.RECEIVE".equals(action)) {
			final String payload = intent.getStringExtra("newsid");
			final String title = intent.getStringExtra("title");
			if (webservice.Readnotification() == 1) {
				if (webservice.Readnotificationpopup() == 1) {
					createNotification(context, payload, title);
					popoutNotification(context, payload, title);
				} else {
					createNotification(context, payload, title);
				}
			}
		}
	}

	private void popoutNotification(Context context, String payload,
			String title) {
		Intent intents = new Intent(context, C2DM_SuperDialog.class);
		Bundle bundle = new Bundle();
		bundle.putString("newsid", payload);
		bundle.putString("title", title);
		intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intents.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intents.putExtras(bundle);
		context.startActivity(intents);
	}

	@SuppressWarnings("deprecation")
	private void createNotification(Context context, String payload,
			String title) {
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(R.drawable.push_logo,
				Config_ConstantVariable.registerpushnotificationtitle,
				System.currentTimeMillis());
		if (webservice.Readnotificationsound() == 1) {
			notification.defaults |= Notification.DEFAULT_SOUND;
		}

		if (webservice.Readnotificationvibrate() == 1) {
			notification.defaults |= Notification.DEFAULT_VIBRATE;
		}

		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		Intent intent = new Intent(context, Main_ParticularNewsDetail.class);
		Bundle bundle = new Bundle();
		intent.putExtra("newsid", payload);
		intent.putExtra("activitynumber", 5);
		intent.putExtras(bundle);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		notification.setLatestEventInfo(context,
				Config_ConstantVariable.latest, title, pendingIntent);
		notificationManager.notify(0, notification);
	}
}
