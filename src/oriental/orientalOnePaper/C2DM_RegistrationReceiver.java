package oriental.orientalOnePaper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class C2DM_RegistrationReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if ("com.google.android.c2dm.intent.REGISTRATION".equals(action)) {
			final String registrationId = intent
					.getStringExtra("registration_id");
			String error = intent.getStringExtra("error");
			String removed = intent.getStringExtra("unregistered");

			if (removed != null) {
				onUnregistrated(context);
				return;
			} else if (error != null) {
				onError(context, error);
				return;
			} else {
				onRegistrated(context, registrationId);
			}
		}
	}

	private void onUnregistrated(Context context) {
		C2DM_Registration.clearRegistrationId(context);
	}

	private void onError(Context context, String errorId) {
		C2DM_Registration.clearRegistrationId(context);
		if ("SERVICE_NOT_AVAILABLE".equals(errorId)) {
			long backoffTimeMs = C2DM_Registration.getBackoff(context);
			Intent retryIntent = new Intent(
					"com.google.android.c2dm.intent.RETRY");
			PendingIntent retryPIntent = PendingIntent.getBroadcast(context, 0,
					retryIntent, 0);
			AlarmManager am = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			am.set(AlarmManager.ELAPSED_REALTIME, backoffTimeMs, retryPIntent);
			backoffTimeMs *= 2;
			C2DM_Registration.setBackoff(context, backoffTimeMs);
		}
	}

	private void onRegistrated(Context context, String registrationId) {
		C2DM_Registration.setRegistrationID(context, registrationId);
	}
}
