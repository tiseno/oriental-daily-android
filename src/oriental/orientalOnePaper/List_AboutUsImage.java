package oriental.orientalOnePaper;

public class List_AboutUsImage {

	int _AboutUsImgID;
	String _AboutUsImgPath;
	String _AboutUsImg_UpdateDate;
	String _AboutUs_Link;

	public List_AboutUsImage() {
	}

	public List_AboutUsImage(int AboutUsImgID) {
		this._AboutUsImgID = AboutUsImgID;
	}

	public List_AboutUsImage(int AboutUsImgID, String AboutUsImagePath,
			String AboutUsImageUpdateDate, String AboutUsLink) {
		this._AboutUsImgID = AboutUsImgID;
		this._AboutUsImgPath = AboutUsImagePath;
		this._AboutUsImg_UpdateDate = AboutUsImageUpdateDate;
		this._AboutUs_Link = AboutUsLink;
	}

	public int getAboutUsID() {
		return this._AboutUsImgID;
	}

	public void setAboutUsImgID(int AboutUsImgIDs) {
		this._AboutUsImgID = AboutUsImgIDs;
	}

	public String getAboutUsImgPath() {
		return this._AboutUsImgPath;
	}

	public void setAboutUsImgPath(String AboutUsImgPaths) {
		this._AboutUsImgPath = AboutUsImgPaths;
	}

	public String getAboutUsImgUpdateDate() {
		return this._AboutUsImg_UpdateDate;
	}

	public void setAboutUsImgUpdateDate(String AboutUsImgUpdateDates) {
		this._AboutUsImg_UpdateDate = AboutUsImgUpdateDates;
	}

	public String getAboutUsLink() {
		return this._AboutUs_Link;
	}

	public void setAboutUsLink(String AboutUsLinks) {
		this._AboutUs_Link = AboutUsLinks;
	}
}