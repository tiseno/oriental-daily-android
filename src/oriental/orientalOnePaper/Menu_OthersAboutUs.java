package oriental.orientalOnePaper;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class Menu_OthersAboutUs extends Activity {
	private Database_WebService webservice;
	private ImageView imageview;
	private String imagepath;
	private ImageLoader_Loader imageloader;
	private Config_ConstantVariable constant;
	private boolean onetime = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_othersaboutus);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		webservice = new Database_WebService(this);
		constant = new Config_ConstantVariable(this);

		Image();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater menulayout = getMenuInflater();
		menulayout.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.setting) {
			Intent intent = new Intent(Menu_OthersAboutUs.this,
					Menu_Setting.class);
			startActivity(intent);
			return true;
		}
		return false;
	}
	
	@Override
	public void onBackPressed() {
		Intent otherintent = new Intent(this, Menu_Others.class);
		startActivity(otherintent);
		finish();
	}

	private void InitialState() {
		if (constant.isOnline()) {
			webservice.UpdateAboutUs();
			new Handler().postDelayed(new Runnable() {
				public void run() {
					Image();
				}
			}, 5 * 1000);
		}
	}

	private void Image() {
		constant.AllCategoryButton( -1, false);
		constant.ParticularCategoryButton(0);
	//	constant.OthersButton(bundle);
		constant.LatestNewsButton();
		imageview = (ImageView) findViewById(R.id.image_aboutus);
		webservice.LoadAboutUsImage();
		try {
			imagepath = Database_WebService.AbtImg.get(0).getAboutUsImgPath();
			imageloader = new ImageLoader_Loader(Menu_OthersAboutUs.this);
			imageloader.DisplayImage(
					Config_ConstantVariable.frontaboutusimagepath + imagepath,
					imageview);
			imageview.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					if (constant.menuOut) {
						constant.Animation();
					} else {
						Intent intent = new Intent(Menu_OthersAboutUs.this,
								Menu_OthersAboutUsWebsite.class);
						Bundle bundle = new Bundle();
						bundle.putString("type", "aboutus");
						intent.putExtras(bundle);
						startActivity(intent);
						finish();
					}
				}
			});
			if (!onetime) {
				onetime = true;
				InitialState();
			}
		} catch (Exception ex) {
			if (constant.isOnline()) {
				constant.StartAnimation(10);
				constant.DisableAllCategoryButton();
				constant.DisableLatestNewsButton();
				constant.DisableOthersButton();
				constant.DisableParticularCategoryButton();
				webservice.UpdateAboutUs();
				new Handler().postDelayed(new Runnable() {
					public void run() {
						Image();
						constant.StopAnimation();
					}
				}, 5 * 1000);
			} else {
				constant.ShowMessage(Config_ConstantVariable.warnmsg_nowifi);
				finish();
			}
		}
	}

}