package oriental.orientalOnePaper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Config_ConstantVariable implements AnimationListener {

	public static final String server = "http://www.zrlim.com/oriental/admin/post.php";
	public static final String frontaboutusimagepath = "http://www.orientaldaily.com.my/ftp/tiseno/AboutUsImage/";
	public static final String sharelink = "http://www.orientaldaily.com.my/index.php?option=com_k2&view=item&id=";

	public static final String sender = "info@tiseno.com";

	private static DateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");
	private static Calendar cal = Calendar.getInstance();
	public static final String date = dateFormat.format(cal.getTime())
			.toString();

	public static final String interested = "興趣 （點擊返回鍵完成選擇）";
	public static final String alertmsgdownload = "需時下載\n確定下載？";
	public static final String alertsuggestion = "確定退出意見箱？";
	public static final String alertnewspaper = "確定取消訂閱報章？";
	public static final String alertadvertisement = "確定取消刊登廣告？";
	public static final String alertmsg = "確定離開？";
	public static final String alertmsgpush = "確定離開推送新聞？";
	public static final String alertbtnyes = "是";
	public static final String alertbtnno = "否";
	public static final String alertbtnproceed = "更多";
	public static final String alertbtndismiss = "退出";
	public static final String alertbtnback = "返回";
	public static final String switchon = "開";
	public static final String switchoff = "關";
	public static final String registerpushnotificationtitle = "東方日報";
	public static final String main = "主頁";

	public static final String interested1 = "保健/醫療";
	public static final String interested2 = "體育/戶外";
	public static final String interested3 = "投資/財經";
	public static final String interested4 = "旅遊/休閒";
	public static final String interested5 = "服裝/時尚";
	public static final String interested6 = "電腦/科技";
	public static final String interested7 = "汽車/產業";
	public static final String interested8 = "購物/音樂";

	public static final String latest = "即時新聞";
	public static final String menu_finance = "財經";
	public static final String menu_special = "專題";
	public static final String menu_comment = "評論";
	public static final String menu_forum = "言論";

	public static final int menu_financecat = 2;
	public static final int menu_specialcat = 7;
	public static final int menu_commentcat = 10;
	public static final int menu_forumcat = 11;

	public static final String digi = "欲订阅服務，輸入BUY《空格》ODGEN，傳送到2000\n欲終止服務，輸入STOP《空格》ODGEN，傳送到2000";
	public static final String maxis = "欲订阅服務，輸入BUY《空格》ODNEWS，傳送到26000\n欲終止服務，輸入STOP《空格》ODNEWS，傳送到26000";
	public static final String celcom = "欲订阅服務，輸入BUY《空格》ODNEWS，傳送到28800\n欲終止服務，輸入STOP《空格》ODNEWS，傳送到28800";
	public static final String umobile = "欲订阅服務，輸入BUY《空格》NW《空格》ORIENTAL，傳送到28118\n欲終止服務，輸入STOP《空格》NW《空格》ORIENTAL，傳送到28118";

	public static final String warnmsg_serverwifidown = "手機還未連接網絡\n請檢查手機網絡！";
	public static final String warnmsg_failedupdate = "下載失敗。。。";
	public static final String warnmsg_wifiserverdownrefresh = "下載失敗。。。";

	public static final String warnmsg_nowifi = "手機還未連接網絡\n請檢查手機網絡！";
	public static final String warnmsg_nowifiautofailed = "手機還未連接網絡\n無法自動下載！";
	public static final String warnmsg_nointerested = "請選擇興趣！";
	public static final String warnmsg_noename = "請填寫姓名！";
	public static final String warnmsg_noemail = "請填寫電郵！";
	public static final String warnmsg_noaddress = "請填寫地址！";
	public static final String warnmsg_nohphone = "請填寫電話號碼！";
	public static final String warnmsg_noadsdetail = "請填寫廣告詳情！";
	public static final String warnmsg_nosuggestion = "請填寫意見";

	public static final String warnmsg_nowifiblocksubmit = "手機還爲連接網絡，無法提交！";
	public static final String warnmsg_surveysubmit = "谢谢填写！";
	public static final String warnmsg_downloading = "正在下載最新資料。。。";
	public static final String warnmsg_retrieving = "正在下載更多最新資料。。。";
	public static final String warnmsg_refreshing = "正在下載最新資料。。。";
	public static final String warnmsg_getting = "正在下載最新資料。。。";
	public static final String warnmsg_nodownloading = "無須下載最新資料。。。";
	public static final String warnmsg_suggestionsubmit = "謝謝意見！";
	public static final String warnmsg_advertisementsubmit = "謝謝刊登廣告";
	public static final String warnmsg_newspapersubmit = "謝謝訂紙";

	public static final String commentgm = "暫無留言";
	public static final String commentgmname = "管理員";
	public static final String commentposter = "匿名者";
	public static final String warnmsg_blocksharenews = "手機還未連接網絡，無法分享新聞！";
	public static final String warnmsg_nocomment = "留言欄不能空白！";
	public static final String warnmsg_commentapprove = "留言待批！";
	public static final String warnmsg_blockpostcomment = "手機還未連接網絡，無法留言！";
	public static final String warnmsg_serverblockpostcomment = "伺服器緩慢或還未連接網絡，請再按留言！";
	public static final String warnmsg_serverblockaccesscomment = "伺服器緩慢或還未連接網絡，無法查閱留言！";
	public static final String warnmsg_nowifidisplaycomment = "手機還未連接網絡，無法進入留言區！";

	public static final String MY_AD_UNIT_ID = "a14fe853330ee80";

	public boolean menuOut = false;

	// ==========================For This Class Only========================//
	private Activity activity;
	private LinearLayout linear, linear2, admoblayout, admoblayout1;
	private RelativeLayout headtitle, home;
	private ImageButton btnnews, btnfinance, btnspecial, btndragon, btnforum,
			btnothers;
	private AdView adView;
	private ListView lv;
	private AlphaAnimation alpha;
	private View footeradmob, linearview, linearview2, headtitleview,
			headtitleview2, menu;
	private AnimParams animParams, animParams2;
	private Database_WebService webservice;
	private GridView gv;
	private Animation anim, anim2;
	private Bundle bundle;
	private int[] Int, temp = { 0 }, catnewsid;
	private String[] string, temptitle = { Config_ConstantVariable.main },
			cattitle;
	private int total;

	// =====================================================================//

	// ===========================Constructor===============================//
	public Config_ConstantVariable(Activity activity) {
		this.activity = activity;
		linear2 = (LinearLayout) activity.findViewById(R.id.layout_loading);
		linear2.setVisibility(View.INVISIBLE);

		linear = (LinearLayout) activity.findViewById(R.id.layout_content);
		linear.setVisibility(View.VISIBLE);

		headtitleview = activity.findViewById(R.id.layout_title);
		headtitleview.setVisibility(View.VISIBLE);

		headtitleview2 = activity.findViewById(R.id.layout_title2);
		headtitleview2.setVisibility(View.INVISIBLE);

		linearview = activity.findViewById(R.id.layout_content);
		linearview.setVisibility(View.VISIBLE);

		linearview2 = activity.findViewById(R.id.layout_menudrag);
		linearview2.setVisibility(View.INVISIBLE);

		menu = activity.findViewById(R.id.layout_menu);

		headtitle = (RelativeLayout) activity.findViewById(R.id.layout_title);
	}

	public Config_ConstantVariable(Activity activity, boolean firsttime) {
		this.activity = activity;

		linear = (LinearLayout) activity.findViewById(R.id.layout_content);
		linear.setVisibility(View.VISIBLE);
	}

	// =============================Get Margin================================//
	@SuppressWarnings("deprecation")
	public LinearLayout.LayoutParams margin(ImageButton btn, int nobtn) {
		Display display = activity.getWindowManager().getDefaultDisplay();
		int width = display.getWidth();
		int gap = (width / 5) - 50;
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) btn
				.getLayoutParams();
		params.setMargins(0, 0, gap, 0);
		return params;
	}

	// =======================================================================//

	// ============================Enable Button==============================//
	public void ParticularCategoryButton(int number) {
		btnfinance = (ImageButton) activity.findViewById(R.id.btn_finance);
		btnfinance.setLayoutParams(margin(btnfinance, 5));
		if (number != 2) {
			btnfinance.setEnabled(true);
			btnfinance.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					StartAnimation(1);
					new Handler().postDelayed(new Runnable() {
						public void run() {
							Intent intent = new Intent(activity,
									Main_ParticularCategoryAllNews.class);
							bundle = new Bundle();
							bundle.putInt("catnewsid", menu_financecat);
							bundle.putString("pagetitle", menu_finance);
							bundle.putInt("position", 2);
							bundle.putBoolean("isshow", false);
							bundle.putInt("scrollposition", 0);
							intent.putExtras(bundle);
							activity.startActivity(intent);
							activity.finish();
							StopAnimation();
						}
					}, 1 * 1000);
				}
			});
		} else {
			btnfinance
					.setBackgroundResource(R.color.button_changeeconomy_active);
		}

		btnspecial = (ImageButton) activity.findViewById(R.id.btn_special);
		btnspecial.setLayoutParams(margin(btnspecial, 5));
		if (number != 7) {
			btnspecial.setEnabled(true);
			btnspecial.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					StartAnimation(1);
					new Handler().postDelayed(new Runnable() {
						public void run() {
							Intent intent = new Intent(activity,
									Main_ParticularCategoryAllNews.class);
							bundle = new Bundle();
							bundle.putInt("catnewsid", menu_specialcat);
							bundle.putString("pagetitle", menu_special);
							bundle.putInt("position", 9);
							bundle.putBoolean("isshow", false);
							bundle.putInt("scrollposition", 0);
							intent.putExtras(bundle);
							activity.startActivity(intent);
							activity.finish();
							StopAnimation();
						}
					}, 1 * 1000);
				}
			});
		} else {
			btnspecial
					.setBackgroundResource(R.color.button_changespecial_active);
		}

		btndragon = (ImageButton) activity.findViewById(R.id.btn_dragon);
		btndragon.setLayoutParams(margin(btndragon, 5));
		if (number != 10) {
			btndragon.setEnabled(true);
			btndragon.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					StartAnimation(1);
					new Handler().postDelayed(new Runnable() {
						public void run() {
							Intent intent = new Intent(activity,
									Main_ParticularCategoryAllNews.class);
							bundle = new Bundle();
							bundle.putInt("catnewsid", menu_commentcat);
							bundle.putString("pagetitle", menu_comment);
							bundle.putInt("position", -1);
							bundle.putBoolean("isshow", false);
							bundle.putInt("scrollposition", 0);
							intent.putExtras(bundle);
							activity.startActivity(intent);
							activity.finish();
							StopAnimation();
						}
					}, 1 * 1000);
				}
			});
		} else {
			btndragon.setBackgroundResource(R.color.button_changeforum_active);
		}

		btnforum = (ImageButton) activity.findViewById(R.id.btn_forum);
		btnforum.setLayoutParams(margin(btnforum, 5));
		if (number != 11) {
			btnforum.setEnabled(true);
			btnforum.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					StartAnimation(1);
					new Handler().postDelayed(new Runnable() {
						public void run() {
							Intent intent = new Intent(activity,
									Main_ParticularCategoryAllNews.class);
							bundle = new Bundle();
							bundle.putInt("catnewsid", menu_forumcat);
							bundle.putString("pagetitle", menu_forum);
							bundle.putInt("position", 5);
							bundle.putBoolean("isshow", false);
							bundle.putInt("scrollposition", 0);
							intent.putExtras(bundle);
							activity.startActivity(intent);
							activity.finish();
							StopAnimation();
						}
					}, 1 * 1000);
				}
			});
		} else {
			btnforum.setBackgroundResource(R.color.button_changediscussion_active);
		}
	}

	public void OthersButton(final Bundle bundle) {
		btnothers = (ImageButton) activity.findViewById(R.id.btn_others);
		btnothers.setEnabled(true);
		btnothers.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				StartAnimation(1);
				new Handler().postDelayed(new Runnable() {
					public void run() {
						Intent otherintent = new Intent(activity,
								Menu_Others.class);
						otherintent.putExtras(bundle);
						activity.startActivity(otherintent);
						activity.finish();
					}
				}, 1 * 1000);

			}
		});
	}

	public void LatestNewsButton() {
		home = (RelativeLayout) activity.findViewById(R.id.layout_home);
		home.setEnabled(true);
		home.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				StartAnimation(1);
				new Handler().postDelayed(new Runnable() {
					public void run() {
						Intent intent = new Intent(activity,
								Main_AllLatestNews.class);
						bundle = new Bundle();
						bundle.putBoolean("isshow", false);
						bundle.putBoolean("needdownload", false);
						bundle.putBoolean("isfirsttime", false);
						bundle.putInt("position", 0);
						intent.putExtras(bundle);
						activity.startActivity(intent);
						activity.finish();
						StopAnimation();
					}
				}, 1 * 1000);
			}
		});
	}

	// ==========================Sidebar Navigation===========================//
	public void AllCategoryButton(final int position, boolean isthispage) {
		btnnews = (ImageButton) activity.findViewById(R.id.btn_news);
		btnnews.setEnabled(true);
		btnnews.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				Animation();
			}
		});

		webservice = new Database_WebService(activity);
		webservice.LoadCategoryTitle();
		try {
			catnewsid = new int[webservice.newsCat.size()];
			cattitle = new String[webservice.newsCat.size()];
			for (int i = 0; i < webservice.newsCat.size(); i++) {
				catnewsid[i] = webservice.newsCat.get(i).getCatID();
				cattitle[i] = webservice.newsCat.get(i).getNewCatName();
			}

			total = temp.length + catnewsid.length;
			string = new String[total];
			Int = new int[total];
			for (int i = 0; i < temp.length; i++) {
				Int[i] = temp[i];
				string[i] = temptitle[i];
			}
			for (int i = 0; i < catnewsid.length; i++) {
				Int[temp.length + i] = catnewsid[i];
				string[temptitle.length + i] = cattitle[i];
			}

			gv = (GridView) activity.findViewById(R.id.gridview);
			gv.setAdapter(new CustomAdapter_AllCategoryNews(activity, cattitle,
					position, isthispage));
			gv.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> arg0, View arg1,
						final int arg2, long arg3) {
					if (position != arg2) {
						if (arg2 == 0) {
							StartAnimation(1);
							new Handler().postDelayed(new Runnable() {
								public void run() {
									Intent intent = new Intent(activity,
											Main_AllLatestNews.class);
									bundle = new Bundle();
									bundle.putBoolean("isshow", false);
									bundle.putBoolean("needdownload", false);
									bundle.putBoolean("isfirsttime", false);
									bundle.putInt("position", arg2);
									intent.putExtras(bundle);
									activity.startActivity(intent);
									activity.finish();
									StopAnimation();
								}
							}, 1 * 1000);
						} else {
							StartAnimation(1);
							new Handler().postDelayed(new Runnable() {
								public void run() {
									Intent intent = new Intent(
											activity,
											Main_ParticularCategoryAllNews.class);
									bundle = new Bundle();
									bundle.putInt("catnewsid", Int[arg2]);
									bundle.putString("pagetitle", string[arg2]);
									bundle.putInt("position", arg2);
									bundle.putBoolean("isshow", false);
									intent.putExtras(bundle);
									activity.startActivity(intent);
									activity.finish();
									StopAnimation();
								}
							}, 1 * 1000);
						}
					} else {
						Animation();
					}
				}
			});
		} catch (Exception ex) {
			AllCategoryButton(-1, false);
		}
	}

	public void Animation() {
		animParams = new AnimParams();
		int w = linearview.getMeasuredWidth();
		int h = linearview.getMeasuredHeight();
		int left = (int) (linearview.getMeasuredWidth() * 0.8);

		animParams2 = new AnimParams();
		int w2 = headtitleview.getMeasuredWidth();
		int h2 = headtitleview.getMeasuredHeight();
		int left2 = (int) (headtitleview.getMeasuredWidth() * 0.8);
		if (!menuOut) {
			anim = new TranslateAnimation(0, left, 0, 0);
			linearview2.setVisibility(View.VISIBLE);
			animParams.init(left, 0, left + w, h);

			anim2 = anim;
			headtitleview2.setVisibility(View.VISIBLE);
			animParams2.init(left2, 0, left2 + w2, h2);
		} else {
			anim = new TranslateAnimation(0, -left, 0, 0);
			animParams.init(0, 0, w, h);

			anim2 = anim;
			animParams2.init(0, 0, w2, h2);
		}
		anim.setDuration(300);
		anim.setAnimationListener(Config_ConstantVariable.this);
		anim.setFillAfter(true);
		linearview.startAnimation(anim);

		anim2.setDuration(300);
		anim2.setAnimationListener(Config_ConstantVariable.this);
		anim2.setFillAfter(true);
		headtitleview.startAnimation(anim2);
	}

	public void onAnimationEnd(Animation animation) {
		menuOut = !menuOut;
		if (!menuOut) {
			linearview2.setVisibility(View.INVISIBLE);
			headtitleview2.setVisibility(View.INVISIBLE);
		} else {
			headtitleview2.setOnTouchListener(new OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
					if (event.getX() > headtitleview2.getWidth() * 0.8) {
						Animation();
					}
					return false;
				}
			});
		}
		layoutApp(menuOut);
	}

	public void onAnimationRepeat(Animation animation) {

	}

	public void onAnimationStart(Animation animation) {

	}

	public void layoutApp(boolean menuOut) {
		linearview.layout(animParams.left, animParams.top, animParams.right,
				animParams.bottom);
		linearview.clearAnimation();
		headtitleview.layout(animParams2.left, animParams2.top,
				animParams2.right, animParams2.bottom);
		headtitleview.clearAnimation();
	}

	// ===========================Disable Button=============================//
	public void DisableParticularCategoryButton() {
		btnfinance = (ImageButton) activity.findViewById(R.id.btn_finance);
		btnfinance.setEnabled(false);
		btnspecial = (ImageButton) activity.findViewById(R.id.btn_special);
		btnspecial.setEnabled(false);
		btndragon = (ImageButton) activity.findViewById(R.id.btn_dragon);
		btndragon.setEnabled(false);
		btnforum = (ImageButton) activity.findViewById(R.id.btn_forum);
		btnforum.setEnabled(false);
	}

	public void DisableOthersButton() {
		btnothers = (ImageButton) activity.findViewById(R.id.btn_others);
		btnothers.setEnabled(false);
	}

	public void DisableAllCategoryButton() {
		btnnews = (ImageButton) activity.findViewById(R.id.btn_news);
		btnnews.setEnabled(false);
	}

	public void DisableLatestNewsButton() {
		home = (RelativeLayout) activity.findViewById(R.id.layout_home);
		home.setEnabled(false);
	}

	// ========================Advertisement Mobile=========================//
	public void AdsMob() {
		admoblayout = (LinearLayout) activity.findViewById(R.id.layout_adsmob);
		adView = new AdView(activity, AdSize.BANNER, MY_AD_UNIT_ID);
		admoblayout.addView(adView);
		adView.loadAd(new AdRequest());
	}

	public void AdsMob1() {
		admoblayout1 = (LinearLayout) activity
				.findViewById(R.id.layout_adsmob1);
		adView = new AdView(activity, AdSize.BANNER, MY_AD_UNIT_ID);
		admoblayout1.addView(adView);
		adView.loadAd(new AdRequest());
	}

	public View AdMob() {
		lv = (ListView) activity.findViewById(android.R.id.list);
		footeradmob = activity.getLayoutInflater().inflate(R.layout.main_admob,
				lv, false);
		admoblayout = (LinearLayout) footeradmob
				.findViewById(R.id.layout_adsmob);
		adView = new AdView(activity, AdSize.BANNER, MY_AD_UNIT_ID);
		admoblayout.addView(adView);
		adView.loadAd(new AdRequest());
		return footeradmob;
	}

	// =========================Loading Animation==========================//
	public void StartAnimation(int duration) {
		linear2.setVisibility(View.VISIBLE);
		alpha = new AlphaAnimation(0.5F, 0.5F);
		alpha.setDuration(duration);
		alpha.setFillAfter(true);
		linear.startAnimation(alpha);
		menu.startAnimation(alpha);
		headtitle.startAnimation(alpha);
		if (ScreenOrientation() == 1)
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		if (ScreenOrientation() == 2) {
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}
	}

	public void StopAnimation() {
		linear2.setVisibility(View.INVISIBLE);
		alpha = new AlphaAnimation(1.0F, 1.0F);
		alpha.setDuration(0);
		alpha.setFillAfter(true);
		linear.startAnimation(alpha);
		menu.startAnimation(alpha);
		headtitle.startAnimation(alpha);
		linear.setVisibility(View.VISIBLE);
		menu.setVisibility(View.VISIBLE);
		headtitle.setVisibility(View.VISIBLE);
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
	}

	// =======================Error Message============================//
	public void ShowMessage(String message) {
		Toast toast = Toast.makeText(activity, message, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,
				0, 0);
		toast.show();
	}

	public void ShowMessageLong(String message) {
		final Toast toast = Toast
				.makeText(activity, message, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,
				0, 0);
		toast.show();
		new CountDownTimer(5000, 1000) {

			public void onTick(long millisUntilFinished) {
				toast.show();
			}

			public void onFinish() {
				toast.show();
			}

		}.start();
	}

	// =======================Screen Resolution========================//
	public int ScreenOrientation() {
		return activity.getResources().getConfiguration().orientation;
	}

	// ==========================Networking============================//
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();

		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	// =======================Animation Class==========================//
	private class AnimParams {
		int left, right, top, bottom;

		void init(int left, int top, int right, int bottom) {
			this.left = left;
			this.top = top;
			this.right = right;
			this.bottom = bottom;
		}
	}
}
