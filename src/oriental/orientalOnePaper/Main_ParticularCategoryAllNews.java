package oriental.orientalOnePaper;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class Main_ParticularCategoryAllNews extends ListActivity {
	private ImageButton btnrefresh;
	private Button btnmore;
	private int catnewsid, newsid[], position = -1, temp, index;
	private Database_WebService webservice;
	private Bundle bundle;
	private ListView lv;
	private String pagetitletext, title[], date[], imagepath[];
	private CustomAdapter_ParticularCategoryAllNews adapter;
	private View footermore;
	private Config_ConstantVariable constant;
	private LinearLayout linear;
	private boolean isshow = false, ispublished;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_particularcategoryallnews);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		bundle = this.getIntent().getExtras();
		catnewsid = bundle.getInt("catnewsid");
		pagetitletext = bundle.getString("pagetitle");
		position = bundle.getInt("position");
		isshow = bundle.getBoolean("isshow");
		index = bundle.getInt("scrollposition");

		webservice = new Database_WebService(this);
		constant = new Config_ConstantVariable(this);

		bundle.putBoolean("isjump", false);
		bundle.putInt("activitynumber", 2);
		bundle.putInt("previousactivitynumber", 2);
		bundle.putInt("catnewsid", catnewsid);
		bundle.putString("pagetitle", pagetitletext);
		bundle.putInt("position", position);
		bundle.putBoolean("isshow", isshow);
		bundle.putInt("scrollposition", index);

		if (isshow) {
			constant.ShowMessage(Config_ConstantVariable.warnmsg_wifiserverdownrefresh);
			Intent intent = new Intent(Main_ParticularCategoryAllNews.this,
					Main_ParticularCategoryAllNews.class);
			intent.putExtras(bundle);
			startActivity(intent);
			finish();
		}

		webservice.LoadCatNews(catnewsid);
		constant.ParticularCategoryButton(catnewsid);
		constant.OthersButton(bundle);
		constant.AllCategoryButton(position, true);
		constant.LatestNewsButton();
		AnimateSidebar();

		btnrefresh = (ImageButton) findViewById(R.id.btn_refresh);
		lv = (ListView) findViewById(android.R.id.list);
		footermore = getLayoutInflater().inflate(R.layout.main_listfooter, lv,
				false);
		btnmore = (Button) footermore.findViewById(R.id.btn_more);

		RefreshParticularCategoryNews();
		filldata();
	}

	@Override
	public void onDestroy() {
		lv = (ListView) findViewById(android.R.id.list);
		lv.setAdapter(null);
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(Config_ConstantVariable.alertmsg)
				.setCancelable(false)
				.setPositiveButton(Config_ConstantVariable.alertbtnyes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								finish();
							}
						})
				.setNegativeButton(Config_ConstantVariable.alertbtnno,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater menulayout = getMenuInflater();
		menulayout.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.setting) {
			Intent intent = new Intent(Main_ParticularCategoryAllNews.this,
					Menu_Setting.class);
			startActivity(intent);
			return true;
		}
		return false;
	}

	private void AnimateSidebar() {
		linear = (LinearLayout) findViewById(R.id.layout_content);
		linear.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (constant.menuOut) {
					constant.Animation();
				}
				return false;
			}
		});
	}

	private void RefreshParticularCategoryNews() {
		btnrefresh.setVisibility(View.VISIBLE);
		btnrefresh.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				if (constant.isOnline()) {
					constant.ShowMessage(Config_ConstantVariable.warnmsg_refreshing);
					constant.StartAnimation(1000);
					btnmore.setEnabled(false);
					btnrefresh.setEnabled(false);
					lv.setEnabled(false);
					webservice = new Database_WebService(
							Main_ParticularCategoryAllNews.this,
							Main_ParticularCategoryAllNews.this, catnewsid,
							pagetitletext, position, 0);
					webservice.refreshCatNewsindex(catnewsid);
					constant.DisableAllCategoryButton();
					constant.DisableParticularCategoryButton();
					constant.DisableOthersButton();
					constant.DisableLatestNewsButton();
				} else {
					constant.ShowMessage(Config_ConstantVariable.warnmsg_nowifi);
				}
			}
		});
	}

	private void filldata() {
		try {
			newsid = new int[webservice.news.size()];
			title = new String[webservice.news.size()];
			date = new String[webservice.news.size()];
			imagepath = new String[webservice.news.size()];
			for (int i = 0; i < webservice.news.size(); i++) {
				newsid[i] = webservice.news.get(i).getID();
				title[i] = webservice.news.get(i).getNtitle();
				date[i] = webservice.news.get(i).getNArticalD();
				imagepath[i] = webservice.news.get(i).getImagePath();
			}
			adapter = new CustomAdapter_ParticularCategoryAllNews(this, title,
					date, imagepath);
			lv.addFooterView(footermore);
			if (constant.isOnline()) {
				lv.addHeaderView(constant.AdMob());
				lv.addFooterView(constant.AdMob());
				ispublished = true;
			} else {
				ispublished = false;
			}
			TextView titletext = (TextView) findViewById(R.id.text_pagetitle);
			titletext.setText(pagetitletext.toString());
			lv.setAdapter(adapter);
			lv.setSelectionFromTop(index, 0);
			lv.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> arg0, View arg1,
						final int arg2, long arg3) {
					if (!constant.menuOut) {
						constant.StartAnimation(1);
						new Handler().postDelayed(new Runnable() {
							public void run() {
								if (ispublished) {
									temp = arg2 - 1;
								} else {
									temp = arg2;
								}
								if (catnewsid != 9) {
									Intent intent = new Intent(
											Main_ParticularCategoryAllNews.this,
											Main_ParticularNewsDetail.class);
									bundle.putInt("newsid", newsid[temp]);
									intent.putExtras(bundle);
									startActivity(intent);
									finish();
									constant.StopAnimation();
								} else {
									Intent intent = new Intent(
											Main_ParticularCategoryAllNews.this,
											Menu_OthersAboutUsWebsite.class);
									bundle.putString("type", "special");
									bundle.putString("link",
											"http://www.orientaldaily.com.my/index.php?option=com_k2&view=item&id="
													+ newsid[temp]
													+ ":&Itemid=223");
									intent.putExtras(bundle);
									startActivity(intent);
									constant.StopAnimation();
								}
							}
						}, 1 * 1000);
					} else {
						constant.Animation();
					}
				}
			});

			btnmore.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					if (!constant.menuOut) {
						if (constant.isOnline()) {
							constant.ShowMessage(Config_ConstantVariable.warnmsg_retrieving);
							constant.StartAnimation(1000);
							btnmore.setEnabled(false);
							btnrefresh.setEnabled(false);
							lv.setEnabled(false);
							index = lv.getFirstVisiblePosition();
							webservice = new Database_WebService(
									Main_ParticularCategoryAllNews.this,
									Main_ParticularCategoryAllNews.this,
									catnewsid, pagetitletext, position, index);
							webservice.UpdateMoreCatNews(catnewsid);
							constant.DisableAllCategoryButton();
							constant.DisableParticularCategoryButton();
							constant.DisableOthersButton();
							constant.DisableLatestNewsButton();
						} else {
							btnmore.setEnabled(true);
							btnrefresh.setEnabled(true);
							lv.setEnabled(true);
							constant.ShowMessage(Config_ConstantVariable.warnmsg_nowifi);
						}
					} else {
						constant.Animation();
					}
				}
			});
		} catch (Exception ex) {
		}
	}
}