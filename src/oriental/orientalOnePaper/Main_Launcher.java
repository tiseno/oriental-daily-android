package oriental.orientalOnePaper;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;

public class Main_Launcher extends Activity {
	private Database_WebService webservice;
	private int check;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		webservice = new Database_WebService(this);
		check = webservice.CheckSurveySubmit();
		int secondsDelayed = 2;
		new Handler().postDelayed(new Runnable() {
			public void run() {
				if (check == 0) {

					startActivity(new Intent(Main_Launcher.this,
							Main_FormNewUser.class));
					finish();
				}
				if (check == 1) {
					Intent intent = new Intent(Main_Launcher.this,
							Main_AllLatestNews.class);
					Bundle bundle = new Bundle();
					bundle.putBoolean("isshow", false);
					bundle.putBoolean("needdownload", true);
					bundle.putBoolean("isfirsttime", false);
					bundle.putInt("position", 0);
					intent.putExtras(bundle);
					startActivity(intent);
					finish();
				}
			}
		}, secondsDelayed * 1000);
	}
}