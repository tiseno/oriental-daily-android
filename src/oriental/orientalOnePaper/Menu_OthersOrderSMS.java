package oriental.orientalOnePaper;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ScrollView;
import android.widget.TextView;

public class Menu_OthersOrderSMS extends Activity {
	private Config_ConstantVariable constant;
	private ScrollView sv;
	private TextView digi, maxis, celcom, umobile;
	private Bundle bundle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_othersordersms);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		bundle = this.getIntent().getExtras();

		Content();
		constant = new Config_ConstantVariable(this);
		constant.AllCategoryButton(-1, false);
		constant.ParticularCategoryButton(0);
		constant.OthersButton(bundle);
		constant.LatestNewsButton();
		AnimateSidebar();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater menulayout = getMenuInflater();
		menulayout.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.setting) {
			Intent intent = new Intent(Menu_OthersOrderSMS.this,
					Menu_Setting.class);
			startActivity(intent);
			return true;
		}
		return false;
	}

	private void AnimateSidebar() {
		sv = (ScrollView) findViewById(R.id.scrollView1);
		sv.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (constant.menuOut) {
					constant.Animation();
				}
				return false;
			}
		});
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, Menu_Others.class);
		intent.putExtras(bundle);
		startActivity(intent);
		finish();
	}

	private void Content() {
		digi = (TextView) findViewById(R.id.text_digi);
		digi.setText(Config_ConstantVariable.digi);

		maxis = (TextView) findViewById(R.id.text_maxis);
		maxis.setText(Config_ConstantVariable.maxis);

		celcom = (TextView) findViewById(R.id.text_celcom);
		celcom.setText(Config_ConstantVariable.celcom);

		umobile = (TextView) findViewById(R.id.text_umobile);
		umobile.setText(Config_ConstantVariable.umobile);
	}
}
