package oriental.orientalOnePaper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TableLayout;

public class Menu_OthersSuggestion extends Activity {
	private EditText cnametext, emailtext, suggestiontext, remarktext;
	private Button submitbtn, cancelbtn;
	private Database_WebService webservice;
	private String cname, email, suggestion, remark;
	private Config_ConstantVariable constant;
	private TableLayout table;
	private ScrollView sv;
	private Bundle bundle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_otherssuggestion);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		bundle = this.getIntent().getExtras();
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		webservice = new Database_WebService(this);
		constant = new Config_ConstantVariable(this);

		constant.AllCategoryButton(-1, false);
		constant.ParticularCategoryButton(0);
		constant.OthersButton(bundle);
		constant.LatestNewsButton();
		AnimateSidebar();

		SubmitButton();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {

				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater menulayout = getMenuInflater();
		menulayout.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.setting) {
			Intent intent = new Intent(Menu_OthersSuggestion.this,
					Menu_Setting.class);
			startActivity(intent);
			return true;
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, Menu_Others.class);
		intent.putExtras(bundle);
		startActivity(intent);
		finish();
	}

	private void AnimateSidebar() {
		table = (TableLayout) findViewById(R.id.table_suggestion);
		table.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (constant.menuOut) {
					constant.Animation();
				}
				return false;
			}
		});

		sv = (ScrollView) findViewById(R.id.scrollView1);
		sv.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (constant.menuOut) {
					constant.Animation();
				}
				return false;
			}
		});
	}

	private void SubmitButton() {
		cnametext = (EditText) findViewById(R.id.text_cname);
		emailtext = (EditText) findViewById(R.id.text_email);
		suggestiontext = (EditText) findViewById(R.id.text_suggestion);
		remarktext = (EditText) findViewById(R.id.text_remark);

		submitbtn = (Button) findViewById(R.id.btn_submit);
		submitbtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (constant.menuOut) {
					constant.Animation();
				} else {
					if (!(emailtext.getText().toString().trim().equals(""))) {
						if (!(suggestiontext.getText().toString().trim()
								.equals(""))) {
							if (constant.isOnline()) {
								cname = cnametext.getText().toString();
								email = emailtext.getText().toString();
								suggestion = suggestiontext.getText()
										.toString();
								remark = remarktext.getText().toString();
								webservice.InsertEnquiry(cname, email,
										suggestion, remark);
								constant.StartAnimation(100);
								new Handler().postDelayed(new Runnable() {
									public void run() {
										if (webservice.success) {
											constant.ShowMessage(Config_ConstantVariable.warnmsg_suggestionsubmit);
											Intent intent = new Intent(
													Menu_OthersSuggestion.this,
													Menu_Others.class);
											intent.putExtras(bundle);
											startActivity(intent);
											finish();
										} else {
											constant.ShowMessage(Config_ConstantVariable.warnmsg_serverwifidown);
											constant.StopAnimation();
										}
									}
								}, 1 * 1000);
							} else {
								constant.ShowMessage(Config_ConstantVariable.warnmsg_nowifi);
							}
						} else {
							constant.ShowMessage(Config_ConstantVariable.warnmsg_nosuggestion);
						}
					} else {
						constant.ShowMessage(Config_ConstantVariable.warnmsg_noemail);
					}
				}
			}
		});

		cancelbtn = (Button) findViewById(R.id.btn_cancel);
		cancelbtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						Menu_OthersSuggestion.this);
				builder.setMessage(Config_ConstantVariable.alertsuggestion)
						.setCancelable(false)
						.setPositiveButton(Config_ConstantVariable.alertbtnyes,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										Intent intent = new Intent(
												Menu_OthersSuggestion.this,
												Menu_Others.class);
										intent.putExtras(bundle);
										startActivity(intent);
										finish();
									}
								})
						.setNegativeButton(Config_ConstantVariable.alertbtnno,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
	}
}
