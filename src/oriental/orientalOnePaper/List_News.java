package oriental.orientalOnePaper;

import android.util.Log;

public class List_News {

	int _Newsid;
	int _NewsCatID;
	String _NewsTitle;
	String _NewsIntroText;
	String _NewsFullText;
	String _NewsArticalDate;
	String _NewsImagePaht;
	String _NewsAuthor;

	int _NewsCatIDS;
	String _NewsCatNames;

	public List_News() {
	}

	public List_News(int Newsid) {
		this._NewsCatID = Newsid;
	}

	public List_News(int Newsid, int NewsCatID) {
		this._Newsid = Newsid;
		this._NewsCatID = NewsCatID;
	}

	public List_News(int Newsid, int NewsCatID, String NewsTitle,
			String NewsIntroText, String NewsFullText, String NewsArticalDate,
			String NewsImagePath, String NewsAuthor) {
		this._Newsid = Newsid;
		this._NewsCatID = NewsCatID;
		this._NewsTitle = NewsTitle;
		this._NewsIntroText = NewsIntroText;
		this._NewsFullText = NewsFullText;
		this._NewsArticalDate = NewsArticalDate;
		this._NewsImagePaht = NewsImagePath;
		this._NewsAuthor = NewsAuthor;
		
		
	}

	public List_News(int Newsid, int NewsCatID, String NewsTitle,
			String NewsIntroText, String NewsFullText, String NewsArticalDate,
			String NewsImagePath, String NewsAuthor, int Newscatids,
			String NewsCatNames) {
		this._Newsid = Newsid;
		this._NewsCatID = NewsCatID;
		this._NewsTitle = NewsTitle;
		this._NewsIntroText = NewsIntroText;
		this._NewsFullText = NewsFullText;
		this._NewsArticalDate = NewsArticalDate;
		this._NewsImagePaht = NewsImagePath;
		this._NewsCatIDS = Newscatids;
		this._NewsCatNames = NewsCatNames;
		this._NewsAuthor = NewsAuthor;
	}
	
	public int getID() {
		return this._Newsid;
	}

	public void SetID(int NewsID) {
		this._Newsid = NewsID;
	}

	public int getCatid() {
		return this._NewsCatID;
	}

	public void SetCatID(int NewsCatID) {
		this._NewsCatID = NewsCatID;
	}

	public void setNtitle(String Newstitle) {
		this._NewsTitle = Newstitle;
	}

	public String getNtitle() {
		return this._NewsTitle;

	}

	public void setNintroText(String NintroText) {
		this._NewsIntroText = NintroText;
	}

	public String getNintrotext() {
		return this._NewsIntroText;
	}

	public void setFullText(String FullText) {
		this._NewsFullText = FullText;
	}

	public String getFullText() {
		return this._NewsFullText;
	}

	public void setNArtical(String ArticalDate) {
		this._NewsArticalDate = ArticalDate;
	}

	public String getNArticalD() {
		return this._NewsArticalDate;
	}

	public void setImagePaht(String ImagePath) {
		this._NewsImagePaht = ImagePath;
	}

	public String getImagePath() {
		return this._NewsImagePaht;
	}

	public void setAuthor(String SAuthor) {
		this._NewsAuthor = SAuthor;
	}

	public String getAuthor() {
		return this._NewsAuthor;
	}

	public int getCatID() {
		return this._NewsCatIDS;
	}

	public void SetCatIDs(int NewsCatIDs) {
		this._NewsCatIDS = NewsCatIDs;
	}

	public String getNewCatName() {
		return this._NewsCatNames;
	}

	public void setNewsCatName(String NewsCategoryNames) {
		this._NewsCatNames = NewsCategoryNames;
	}
}
