package oriental.orientalOnePaper;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class C2DM_Registration {
	public static void unregister(Context context) {
		Intent unregIntent = new Intent(
				"com.google.android.c2dm.intent.UNREGISTER");
		unregIntent.putExtra("app",
				PendingIntent.getBroadcast(context, 0, new Intent(), 0));
		context.startService(unregIntent);
	}

	public static void register(Context context, String SenderID) {
		Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
		intent.putExtra("app",
				PendingIntent.getBroadcast(context, 0, new Intent(), 0));
		intent.putExtra("sender", "898173928485");
		context.startService(intent);
	}

	static void setRegistrationID(Context context, String registrationId) {
		final SharedPreferences prefs = context.getSharedPreferences(
				"c2dm_preference", Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString("dm_registration", registrationId);
		editor.commit();		
	}

	public static String getRegistrationID(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(
				"c2dm_preference", Context.MODE_PRIVATE);
		String registrationId = prefs.getString("dm_registration", "");
		return registrationId;
	}

	public static void clearRegistrationId(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(
				"c2dm_preference", Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString("dm_registration", "");
		editor.commit();
	}

	public static long getBackoff(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(
				"c2dm_preference", Context.MODE_PRIVATE);
		return prefs.getLong("back_off", 30000);
	}

	public static void setBackoff(Context context, long backoff) {
		final SharedPreferences prefs = context.getSharedPreferences(
				"c2dm_preference", Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putLong("back_off", backoff);
		editor.commit();
	}
}
