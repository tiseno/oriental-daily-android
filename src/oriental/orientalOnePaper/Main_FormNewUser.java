package oriental.orientalOnePaper;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

public class Main_FormNewUser extends Activity {

	private Spinner age, gender, job, salary;
	private EditText nametext, emailtext;
	private Button submitbtn, interest;
	private String name, email, ages, genders, jobs, salaries, deviceId;
	private Database_WebService webservice;
	private Config_ConstantVariable constant;
	private int fontsize = 15;
	private Bundle bundle;

	private LinearLayout linear, linear2;
	private CharSequence[] colours = { Config_ConstantVariable.interested1,
			Config_ConstantVariable.interested2,
			Config_ConstantVariable.interested3,
			Config_ConstantVariable.interested4,
			Config_ConstantVariable.interested5,
			Config_ConstantVariable.interested6,
			Config_ConstantVariable.interested7,
			Config_ConstantVariable.interested8 };
	private ArrayList<CharSequence> selectedColours = new ArrayList<CharSequence>();
	private StringBuilder stringBuilder;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_formnewuser);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		webservice = new Database_WebService(this, this);
		constant = new Config_ConstantVariable(this, true);
		stringBuilder = new StringBuilder();
		linear = (LinearLayout) findViewById(R.id.layout_content);
		linear2 = (LinearLayout) findViewById(R.id.layout_loading);

		deviceId = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
		C2DM_Registration.register(Main_FormNewUser.this,
				Config_ConstantVariable.sender);

		SubmitButton();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP
					&& (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
							.getBottom())) {

				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
						.getWindowToken(), 0);
			}
		}
		return ret;
	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(Config_ConstantVariable.alertmsg)
				.setCancelable(false)
				.setPositiveButton(Config_ConstantVariable.alertbtnyes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								finish();
							}
						})
				.setNegativeButton(Config_ConstantVariable.alertbtnno,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void SubmitButton() {
		age = (Spinner) findViewById(R.id.text_age);
		gender = (Spinner) findViewById(R.id.text_gender);
		job = (Spinner) findViewById(R.id.text_job);
		salary = (Spinner) findViewById(R.id.text_salary);
		interest = (Button) findViewById(R.id.text_interest);

		nametext = (EditText) findViewById(R.id.text_name);
		emailtext = (EditText) findViewById(R.id.text_email);

		age.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> adapterView, View view,
					int i, long l) {
				ages = age.getSelectedItem().toString();
			}

			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});

		gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> adapterView, View view,
					int i, long l) {
				genders = gender.getSelectedItem().toString();
			}

			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});

		job.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> adapterView, View view,
					int i, long l) {
				jobs = job.getSelectedItem().toString();
			}

			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});

		salary.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> adapterView, View view,
					int i, long l) {
				salaries = salary.getSelectedItem().toString();
			}

			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});

		interest.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				showSelectColoursDialog();
			}

		});

		submitbtn = (Button) findViewById(R.id.btn_submit);
		submitbtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!(nametext.getText().toString().trim().equals(""))) {
					if (!(emailtext.getText().toString().trim().equals(""))) {
						if (!(stringBuilder.toString() == "")) {
							if (constant.isOnline()) {
								submitbtn.setEnabled(false);
								linear.setVisibility(View.INVISIBLE);
								linear2.setVisibility(View.VISIBLE);
								AlphaAnimation alpha = new AlphaAnimation(0.5F,
										0.5F);
								alpha.setDuration(1);
								alpha.setFillAfter(true);
								linear.startAnimation(alpha);

								name = nametext.getText().toString();
								if (Character.isLetter(name.charAt(0))) {
									name = name.replace(name.charAt(0),
											Character.toUpperCase(name.charAt(0)));
								}
								email = emailtext.getText().toString();
								webservice.AllinOne(
										name,
										email,
										ages,
										genders,
										jobs,
										salaries,
										stringBuilder.toString(),
										deviceId,
										C2DM_Registration.getRegistrationID(
												Main_FormNewUser.this)
												.toString());								
								webservice.insertFontSize(fontsize);
								webservice.InsertFormSubmit(
										1,
										1,
										1,
										1,
										1,
										C2DM_Registration.getRegistrationID(
												Main_FormNewUser.this)
												.toString());
								constant.ShowMessage(Config_ConstantVariable.warnmsg_surveysubmit);
								Intent intent = new Intent(
										Main_FormNewUser.this,
										Main_AllLatestNews.class);
								bundle = new Bundle();
								bundle.putBoolean("isshow", false);
								bundle.putBoolean("needdownload", true);
								bundle.putBoolean("isfirsttime", true);
								bundle.putInt("position", 0);
								intent.putExtras(bundle);
								startActivity(intent);
								finish();
							} else {
								submitbtn.setEnabled(true);
								constant.ShowMessage(Config_ConstantVariable.warnmsg_serverwifidown);
							}
						} else {
							constant.ShowMessage(Config_ConstantVariable.warnmsg_nointerested);
						}
					} else {
						constant.ShowMessage(Config_ConstantVariable.warnmsg_noemail);
					}
				} else {
					constant.ShowMessage(Config_ConstantVariable.warnmsg_noename);
				}
			}
		});
	}

	protected void showSelectColoursDialog() {
		boolean[] checkedColours = new boolean[colours.length];
		int count = colours.length;

		for (int i = 0; i < count; i++)
			checkedColours[i] = selectedColours.contains(colours[i]);

		DialogInterface.OnMultiChoiceClickListener coloursDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
			public void onClick(DialogInterface dialog, int which,
					boolean isChecked) {
				if (isChecked)
					selectedColours.add(colours[which]);
				else
					selectedColours.remove(colours[which]);

				onChangeSelectedColours();
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(Config_ConstantVariable.interested);
		builder.setMultiChoiceItems(colours, checkedColours,
				coloursDialogListener);

		AlertDialog dialog = builder.create();
		dialog.show();
	}

	protected void onChangeSelectedColours() {
		stringBuilder.delete(0, stringBuilder.length());
		for (CharSequence colour : selectedColours)
			stringBuilder.append(colour + ",");

		interest.setText(stringBuilder.toString());
	}
}
