package oriental.orientalOnePaper;

public class List_NewsComment {

	int _CommentID;
	String _CommentDate;
	int _ItemID;
	String _UserNameComment;
	String _CommentContent;
	int _AproveStatus;

	public List_NewsComment() {
	}

	public List_NewsComment(int CommentID, String CommentyDate, int ItemID,
			String UserName, String CommentContent, int AproveStatus) {
		this._CommentID = CommentID;
		this._CommentDate = CommentyDate;
		this._ItemID = ItemID;
		this._UserNameComment = UserName;
		this._CommentContent = CommentContent;
		this._AproveStatus = AproveStatus;
	}

	public int getcommentID() {
		return this._CommentID;
	}

	public void setcommentID(int CommentID) {
		this._CommentID = CommentID;
	}

	public String getCommentDate() {
		return this._CommentDate;
	}

	public void setcommentDate(String CommentDate) {
		this._CommentDate = CommentDate;
	}

	public int getItemID() {
		return this._ItemID;
	}

	public void setItemID(int ItemID) {
		this._ItemID = ItemID;
	}

	public String getUserName() {
		return this._UserNameComment;
	}

	public void setUserName(String UserNameCommment) {
		this._UserNameComment = UserNameCommment;
	}

	public String getCommentContent() {
		return this._CommentContent;
	}

	public void setCommentContent(String CommentContent) {
		this._CommentContent = CommentContent;
	}

	public int getAproveStatus() {
		return this._AproveStatus;
	}

	public void setAproveStauts(int AprovesStatus) {
		this._AproveStatus = AprovesStatus;
	}

}
