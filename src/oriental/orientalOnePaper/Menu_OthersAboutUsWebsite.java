package oriental.orientalOnePaper;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;

public class Menu_OthersAboutUsWebsite extends Activity {
	private String type, url;
	private Bundle bundle;
	private ImageButton btnnews, btnback;
	private ImageView home;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_othersaboutuswebsite);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		bundle = this.getIntent().getExtras();
		type = bundle.getString("type");
		
		home = (ImageView) findViewById(R.id.image_home);
		home.setVisibility(View.GONE);

		WebView wv = (WebView) findViewById(R.id.webview1);
		wv.getSettings().setBuiltInZoomControls(true);
		wv.getSettings().setSupportZoom(true);
		wv.getSettings().setJavaScriptEnabled(true);

		if (type.equals("aboutus")) {
			url = "http://www.orientaldaily.com.my";
			wv.loadUrl(url);
		}
		if (type.equals("special")) {
			url = bundle.getString("link");
			wv.loadUrl(url);
		}

		Button();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater menulayout = getMenuInflater();
		menulayout.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.setting) {
			Intent intent = new Intent(Menu_OthersAboutUsWebsite.this,
					Menu_Setting.class);
			startActivity(intent);
			return true;
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		if (type.equals("aboutus")) {
			Intent intent = new Intent(Menu_OthersAboutUsWebsite.this,
					Menu_Others.class);
			intent.putExtras(bundle);
			startActivity(intent);
			finish();
		} else {
			finish();
		}
	}

	private void Button() {
		btnnews = (ImageButton) findViewById(R.id.btn_news);
		btnnews.setVisibility(View.GONE);

		btnback = (ImageButton) findViewById(R.id.btn_back);
		btnback.setVisibility(View.VISIBLE);
		btnback.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				if (type.equals("aboutus")) {
					Intent intent = new Intent(Menu_OthersAboutUsWebsite.this,
							Menu_Others.class);
					intent.putExtras(bundle);
					startActivity(intent);
					finish();
				} else {
					finish();
				}
			}
		});
	}
}
