package oriental.orientalOnePaper;

import android.app.Activity;
import android.content.Context;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CustomAdapter_LatestNews extends BaseAdapter {

	private Activity activity;
	private String[] title, category, date, imagepath;
	private static LayoutInflater inflater = null;
	private ImageLoader_Loader imageLoader;
	private WindowManager wm = null;
	private Display display;

	public CustomAdapter_LatestNews(Activity a, String[] title,
			String[] category, String[] date, String[] imagepath) {
		activity = a;
		this.title = title;
		this.category = category;
		this.date = date;
		this.imagepath = imagepath;		
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
		imageLoader = new ImageLoader_Loader(activity.getApplicationContext());
	}

	public int getCount() {
		return title.length;
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("deprecation")
	public View getView(final int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.main_alllatestnewslist, parent,
					false);

		LinearLayout linear = (LinearLayout) vi.findViewById(R.id.layout_image);
		ImageView imageview = (ImageView) vi
				.findViewById(R.id.image_alllatestnewstitle);
		imageview.setVisibility(View.GONE);

		TextView titletext = (TextView) vi
				.findViewById(R.id.text_particularlatestnewstitle);
		TextView categorytext = (TextView) vi
				.findViewById(R.id.text_newscategorytitle);
		TextView datetext = (TextView) vi.findViewById(R.id.text_newsdate);

		if (!imagepath[position].toString().equals("no picture")) {
			imageLoader.DisplayImage(imagepath[position], imageview);
			imageview.setVisibility(View.VISIBLE);
			linear.setVisibility(View.VISIBLE);
		} else {
			imageview.setVisibility(View.GONE);
			imageview.setImageDrawable(null);
			linear.setVisibility(View.GONE);
			display = wm.getDefaultDisplay();
			int screenWidth = display.getWidth();
			titletext.setWidth(screenWidth);
		}
		titletext.setText(title[position].toString());
		categorytext.setText(category[position].toString());
		datetext.setText(date[position].toString());

		return vi;
	}
}
