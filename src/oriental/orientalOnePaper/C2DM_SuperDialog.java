package oriental.orientalOnePaper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

public class C2DM_SuperDialog extends Activity implements
		DialogInterface.OnCancelListener {

	private String newsid, title;
	private Bundle bundle;
	private AlertDialog alert;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bundle = this.getIntent().getExtras();
		newsid = bundle.getString("newsid");
		title = bundle.getString("title");
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(Config_ConstantVariable.latest)
				.setIcon(R.drawable.push_logo)
				.setMessage(title)
				.setCancelable(false)
				.setPositiveButton(Config_ConstantVariable.alertbtnproceed,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Intent intent = new Intent(
										C2DM_SuperDialog.this,
										Main_ParticularNewsDetail.class);
								intent.putExtra("newsid", newsid);
								intent.putExtra("activitynumber", 5);
								intent.putExtras(bundle);
								startActivity(intent);
								finish();
							}
						})
				.setNegativeButton(Config_ConstantVariable.alertbtndismiss,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								finish();
							}
						});
		alert = builder.create();
		alert.show();
	}

	public void onCancel(DialogInterface dialog) {
		finish();
	}
}
