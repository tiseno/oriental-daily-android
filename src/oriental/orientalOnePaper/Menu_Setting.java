package oriental.orientalOnePaper;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.TextView;

public class Menu_Setting extends Activity {
	private CheckBox statusswitch, soundswitch, vibrateswitch, popupswitch;
	private TextView statustext, soundtext, vibratetext, popuptext;
	private Database_WebService webservice;
	private int notistatus, notisound, notivibrate, notipopup;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_setting);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		webservice = new Database_WebService(this);
		NotificationStatus();
		NotificationPopup();
		NotificationSound();
		NotificationVibrate();
	}

	private void NotificationStatus() {
		notistatus = webservice.Readnotification();
		statustext = (TextView) findViewById(R.id.status);
		statusswitch = (CheckBox) findViewById(R.id.btn_statusswitch);

		if (notistatus == 1) {
			statusswitch.setChecked(true);
			statustext.setText(Config_ConstantVariable.switchon);
		}
		if (notistatus == 0) {
			statusswitch.setChecked(false);
			statustext.setText(Config_ConstantVariable.switchoff);
		}

		statusswitch.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				notistatus = webservice.Readnotification();
				if (notistatus == 1) {
					webservice.Updatenotification(0);
					statusswitch.setChecked(false);
					statustext.setText(Config_ConstantVariable.switchoff);
				}
				if (notistatus == 0) {
					webservice.Updatenotification(1);
					statusswitch.setChecked(true);
					statustext.setText(Config_ConstantVariable.switchon);
				}
			}

		});
	}

	private void NotificationPopup() {
		notipopup = webservice.Readnotificationpopup();		
		popuptext = (TextView) findViewById(R.id.popup);
		popupswitch = (CheckBox) findViewById(R.id.btn_popupswitch);

		if (notipopup == 1) {
			popupswitch.setChecked(true);
			popuptext.setText(Config_ConstantVariable.switchon);
		}
		if (notipopup == 0) {
			popupswitch.setChecked(false);
			popuptext.setText(Config_ConstantVariable.switchoff);
		}

		popupswitch.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				notipopup = webservice.Readnotificationpopup();
				if (notipopup == 1) {
					webservice.Updatenotificationpopup(0);
					popupswitch.setChecked(false);
					popuptext.setText(Config_ConstantVariable.switchoff);
				}
				if (notipopup == 0) {
					webservice.Updatenotificationpopup(1);
					popupswitch.setChecked(true);
					popuptext.setText(Config_ConstantVariable.switchon);
				}
			}

		});
	}
	
	private void NotificationSound() {
		notisound = webservice.Readnotificationsound();
		soundtext = (TextView) findViewById(R.id.sound);
		soundswitch = (CheckBox) findViewById(R.id.btn_soundswitch);

		if (notisound == 1) {
			soundswitch.setChecked(true);
			soundtext.setText(Config_ConstantVariable.switchon);
		}
		if (notisound == 0) {
			soundswitch.setChecked(false);
			soundtext.setText(Config_ConstantVariable.switchoff);
		}

		soundswitch.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				notisound = webservice.Readnotificationsound();
				if (notisound == 1) {
					webservice.Updatenotificationsound(0);
					soundswitch.setChecked(false);
					soundtext.setText(Config_ConstantVariable.switchoff);
				}
				if (notisound == 0) {
					webservice.Updatenotificationsound(1);
					soundswitch.setChecked(true);
					soundtext.setText(Config_ConstantVariable.switchon);
				}
			}

		});
	}

	private void NotificationVibrate() {
		notivibrate = webservice.Readnotificationvibrate();
		vibratetext = (TextView) findViewById(R.id.vibrate);
		vibrateswitch = (CheckBox) findViewById(R.id.btn_vibrateswitch);

		if (notivibrate == 1) {
			vibrateswitch.setChecked(true);
			vibratetext.setText(Config_ConstantVariable.switchon);
		}
		if (notivibrate == 0) {
			vibrateswitch.setChecked(false);
			vibratetext.setText(Config_ConstantVariable.switchoff);
		}

		vibrateswitch.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				notivibrate = webservice.Readnotificationvibrate();
				if (notivibrate == 1) {
					webservice.Updatenotificationvibrate(0);
					vibrateswitch.setChecked(false);
					vibratetext.setText(Config_ConstantVariable.switchoff);
				}
				if (notivibrate == 0) {
					webservice.Updatenotificationvibrate(1);
					vibrateswitch.setChecked(true);
					vibratetext.setText(Config_ConstantVariable.switchon);
				}
			}

		});
	}
}
