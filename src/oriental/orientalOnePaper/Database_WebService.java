package oriental.orientalOnePaper;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class Database_WebService {

	public final static String NAMESPACE = "OrientalServices1";
	public final static String URL = "http://www.orientaldaily.com.my/ftp/tiseno/ws/OrientalServices1.php";

	private final String ALLTopNews_SOAP_ACTION = "OrientalServices1#GetTopNewsByCategoryOverall";
	private final String ALLTopNews_METHOD_NAME = "GetTopNewsByCategoryOverall";

	private final static String Cat_SOAP_ACTION = "OrientalServices1#Get_Category";
	private final static String Cat_METHOD_NAME = "Get_Category";

	private final String Survey_SOAP_ACTION = "OrientalServices1#Insert_survey";
	private final String Survey_METHOD_NAME = "Insert_survey";

	private final String Order_SOAP_ACTION = "OrientalServices1#Insert_order";
	private final String Order_METHOD_NAME = "Insert_order";

	private final String Enquiry_SOAP_ACTION = "OrientalServices1#Insert_enquiry";
	private final String Enquiry_METHOD_NAME = "Insert_enquiry";

	private final String Comment_SOAP_ACTION = "OrientalServices1#Insert_Comment";
	private final String Comment_METHOD_NAME = "Insert_Comment";

	private final static String get_AboutUsImage_SOAP_ACTION = "OrientalServices1#Get_AboutusImage";
	private final static String get_AboutUsImage_METHOD_NAME = "Get_AboutusImage";

	private final static String get_moreNews_SOAP_ACTION = "OrientalServices1#GetTopNewsByCategory";
	private final static String get_moreNews_METHOD_NAME = "GetTopNewsByCategory";

	private final static String insertTokenDevice_SOAP_ACTION = "OrientalServices1#responseInsertTokenDevice";
	private final static String insertTokenDevice_METHOD_NAME = "insertTokenDevice";

	private final static String AllinOne_SOAP_ACTION = "OrientalServices1#insert_survey_token";
	private final static String AllinOne_METHOD_NAME = "insert_survey_token";

	public List<List_CategoryNews> newsCat;
	public List<List_News> news;
	public static List<List_UserDetails> form;
	public static List<List_AboutUsImage> AbtImg;

	private Context mContext;
	private Database_DataBaseHelper dbhelper;

	public boolean success = false;
	private Activity activity;
	private Bundle bundle;
	private String pagetitletext;
	private int catnewsid, position, index, newsid;

	public Database_WebService(Context context, Activity activity) {
		this.mContext = context;
		this.activity = activity;
		dbhelper = new Database_DataBaseHelper(this.mContext);
	}

	public Database_WebService(Context context) {
		this.mContext = context;
		dbhelper = new Database_DataBaseHelper(this.mContext);
	}

	public Database_WebService(Context context, Activity activity, int newsid) {
		this.mContext = context;
		this.activity = activity;
		this.newsid = newsid;
		dbhelper = new Database_DataBaseHelper(this.mContext);
	}

	public Database_WebService(Context context, Activity activity,
			int catnewsid, String pagetitletext, int position, int index) {
		this.mContext = context;
		this.activity = activity;
		this.catnewsid = catnewsid;
		this.pagetitletext = pagetitletext;
		this.position = position;
		this.index = index;
		dbhelper = new Database_DataBaseHelper(this.mContext);
	}

	// ==========Online Webservice=============//
	public void GetNewsCategory(final int activitynumber) {
		Thread networkThread = new Thread() {
			@Override
			public void run() {

				try {
					SoapObject request = new SoapObject(NAMESPACE,
							Cat_METHOD_NAME);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					HttpTransportSE androidHttpTransportSE = new HttpTransportSE(
							URL);

					envelope.setOutputSoapObject(request);
					androidHttpTransportSE.call(Cat_SOAP_ACTION, envelope);

					SoapObject result = (SoapObject) envelope.bodyIn;
					Vector<Object> property2 = extracted(result);

					String[] results2 = null;

					for (int i = 0; i < property2.size(); i++) {

						results2 = new String[property2.size()];
						results2[i] = property2.elementAt(i).toString();

						SoapObject getPropertyD = (SoapObject) property2.get(i);

						String getPropertyID = getPropertyD.getProperty("id")
								.toString();

						String getPropertyName = getPropertyD.getProperty(
								"name").toString();

						List_CategoryNews addNewsCat = new List_CategoryNews(
								Integer.parseInt(getPropertyID),
								getPropertyName);
						dbhelper.AddCatNews(addNewsCat);
					}
					refreshTopNews(activitynumber);
				} catch (Exception ex) {

				}
			}
		};
		networkThread.start();
	}

	public void GetAboutUsImage() {
		Thread networkThread = new Thread() {
			@Override
			public void run() {

				try {
					SoapObject request = new SoapObject(NAMESPACE,
							get_AboutUsImage_METHOD_NAME);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					HttpTransportSE androidHttpTransportSE = new HttpTransportSE(
							URL);

					envelope.setOutputSoapObject(request);
					androidHttpTransportSE.call(get_AboutUsImage_SOAP_ACTION,
							envelope);

					SoapObject result = (SoapObject) envelope.bodyIn;
					Vector<Object> property2 = extracted(result);

					for (int i = 0; i < property2.size(); i++) {

						SoapObject getPropertyD = (SoapObject) property2.get(i);

						String getAboutUsImage = getPropertyD.getProperty("id")
								.toString();
						String getpath = getPropertyD.getProperty("path")
								.toString();
						String getupdated_date = getPropertyD.getProperty(
								"updated_date").toString();
						String getlink = getPropertyD.getProperty("link")
								.toString();

						List_AboutUsImage addAbtImg = new List_AboutUsImage(
								Integer.parseInt(getAboutUsImage), getpath,
								getupdated_date, getlink);
						dbhelper.AddAboutUs(addAbtImg);
					}
				} catch (Exception ex) {

				}
			}
		};
		networkThread.start();

	}

	public void GetUpdateAboutUsImage() {
		Thread networkThread = new Thread() {
			@Override
			public void run() {

				try {
					SoapObject request = new SoapObject(NAMESPACE,
							get_AboutUsImage_METHOD_NAME);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					HttpTransportSE androidHttpTransportSE = new HttpTransportSE(
							URL);

					envelope.setOutputSoapObject(request);
					androidHttpTransportSE.call(get_AboutUsImage_SOAP_ACTION,
							envelope);

					SoapObject result = (SoapObject) envelope.bodyIn;
					Vector<Object> property2 = extracted(result);

					for (int i = 0; i < property2.size(); i++) {

						SoapObject getPropertyD = (SoapObject) property2.get(i);

						String getAboutUsImage = getPropertyD.getProperty("id")
								.toString();
						String getpath = getPropertyD.getProperty("path")
								.toString();
						String getupdated_date = getPropertyD.getProperty(
								"updated_date").toString();
						String getlink = getPropertyD.getProperty("link")
								.toString();

						List_AboutUsImage addAbtImg = new List_AboutUsImage(
								Integer.parseInt(getAboutUsImage), getpath,
								getupdated_date, getlink);
						dbhelper.updateAboutUs(addAbtImg);
					}

				} catch (Exception ex) {

				}
			}
		};
		networkThread.start();
	}

	public void GetMoreCatNews(final int CatID, final int IndexID) {
		Thread networkThread = new Thread() {
			@Override
			public void run() {
				try {
					SoapObject request = new SoapObject(NAMESPACE,
							get_moreNews_METHOD_NAME);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					HttpTransportSE androidHttpTransportSE = new HttpTransportSE(
							URL);

					request.addProperty("id", CatID);
					request.addProperty("fromIndex", IndexID);
					envelope.setOutputSoapObject(request);
					androidHttpTransportSE.call(get_moreNews_SOAP_ACTION,
							envelope);

					SoapObject result = (SoapObject) envelope.bodyIn;
					RetrieveMoreCatNewsFromSoap(result, CatID);
				} catch (Exception ex) {
					Intent intent = new Intent(activity,
							Main_ParticularCategoryAllNews.class);
					bundle = new Bundle();
					bundle.putInt("catnewsid", catnewsid);
					bundle.putString("pagetitle", pagetitletext);
					bundle.putInt("position", position);
					bundle.putBoolean("isshow", true);
					bundle.putInt("scrollposition", index);
					intent.putExtras(bundle);
					activity.startActivity(intent);
					activity.finish();
				}
			}
		};
		networkThread.start();
	}

	public void GetAllTopNews(final int activitynumber) {
		Thread networkThread = new Thread() {
			@Override
			public void run() {
				try {
					SoapObject request = new SoapObject(NAMESPACE,
							ALLTopNews_METHOD_NAME);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					HttpTransportSE androidHttpTransportSE = new HttpTransportSE(
							URL);
					envelope.setOutputSoapObject(request);
					androidHttpTransportSE.call(ALLTopNews_SOAP_ACTION,
							envelope);

					SoapObject result = (SoapObject) envelope.bodyIn;

					RetrieveAllTopNewsFromSoap(result, activitynumber);
				} catch (Exception ex) {
					Intent intent = new Intent(activity,
							Main_AllLatestNews.class);
					bundle = new Bundle();
					bundle.putBoolean("isshow", true);
					bundle.putBoolean("needdownload", false);
					bundle.putBoolean("isfirsttime", false);
					bundle.putInt("position", 0);
					intent.putExtras(bundle);
					activity.startActivity(intent);
					activity.finish();
				}
			}
		};
		networkThread.start();
	}

	public void UpdateAllCatNews(final int activitynumber) {
		try {
			dbhelper.DeleteNewsCatFT();
			List<List_CategoryNews> newsCat = dbhelper.getAllNewsCategories();
			if (newsCat.size() == 0) {
				GetNewsCategory(activitynumber);
			}
		} catch (Exception ex) {
		}
	}

	public void UpdateAboutUs() {
		try {
			AbtImg = dbhelper.getAboutUs();
			if (AbtImg.size() == 0) {
				GetAboutUsImage();

			} else if (AbtImg.size() != 0) {
				GetUpdateAboutUsImage();
			}

		} catch (Exception ex) {
		}
	}

	public void UpdateMoreCatNews(int NewsCatID) {
		try {
			news = dbhelper.getSingleCatNews(NewsCatID);
		} catch (Exception e) {
		}
		GetMoreCatNews(NewsCatID, news.size());
	}

	// ==========Refresh Webservice=============//
	public void refreshCatNewsindex(int NewsCatID) {
		try {
			dbhelper.DeleteNewsByCatID(NewsCatID);
			news = dbhelper.getSingleCatNews(NewsCatID);
		} catch (Exception e) {
		}
		GetMoreCatNews(NewsCatID, news.size());
	}

	public void refreshTopNews(final int activitynumber) {
		try {
			dbhelper.DeleteNewsFT();
			news = dbhelper.getAllTodaysNews();
			if (news.size() == 0) {
				GetAllTopNews(activitynumber);
			}
		} catch (Exception e) {
		}

	}

	// ==========Offline Webservice=============//
	public void LoadCategoryTitle() {

		try {
			newsCat = dbhelper.getAllNewsCategories();
		} catch (Exception ex) {
		}
	}

	public void LoadAllNews() {
		try {
			news = dbhelper.getAllNews();
		} catch (Exception e) {
		}

	}

	public void LoadCatNews(int NewsCatID) {
		try {
			news = dbhelper.getSingleCatNews(NewsCatID);
		} catch (Exception e) {
		}

	}

	public void LoadtodayNews() {
		try {
			news = dbhelper.getAllTodaysNews();
		} catch (Exception e) {
		}

	}

	public void LoadSingleNews(int NewsID) {
		try {
			news = dbhelper.getSingleNews(NewsID);
		} catch (Exception e) {
		}
	}

	public void LoadAboutUsImage() {
		try {
			AbtImg = dbhelper.getAboutUs();
		} catch (Exception ex) {

		}
	}
	
	public void DeleteNews() {
		try {
			dbhelper.DeleteNewsFT();
		} catch (Exception ex) {

		}
	}

	// ==========Insert Webservice=============//
	public void AllinOne(final String Name, final String Email,
			final String Age, final String Gender, final String Job,
			final String income, final String Interest, final String devid,
			final String regid) {
		Thread networkThread = new Thread() {
			@Override
			public void run() {

				try {
					SoapObject request = new SoapObject(NAMESPACE,
							AllinOne_METHOD_NAME);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					HttpTransportSE androidHttpTransportSE = new HttpTransportSE(
							URL);

					request.addProperty("name", Name);
					request.addProperty("email", Email);
					request.addProperty("age", Age);
					request.addProperty("gender", Gender);
					request.addProperty("job", Job);
					request.addProperty("income", income);
					request.addProperty("interest", Interest);
					request.addProperty("devid", devid);
					request.addProperty("regid", regid);

					System.out.println(devid);
					System.out.println(regid);

					envelope.setOutputSoapObject(request);
					androidHttpTransportSE.call(AllinOne_SOAP_ACTION, envelope);

					SoapObject result = (SoapObject) envelope.bodyIn;
					Vector<Object> property2 = extracted(result);

					String[] results2 = null;

					for (int i = 0; i < property2.size(); i++) {

						results2 = new String[property2.size()];
						results2[i] = property2.elementAt(i).toString();
					}
				} catch (Exception ex) {

				}
			}
		};
		networkThread.start();
	}

	public void InsertSurvey(final String Name, final String Email,
			final String Age, final String Gender, final String Job,
			final String income, final String Interest) {
		Thread networkThread = new Thread() {
			@Override
			public void run() {

				try {
					SoapObject request = new SoapObject(NAMESPACE,
							Survey_METHOD_NAME);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					HttpTransportSE androidHttpTransportSE = new HttpTransportSE(
							URL);

					request.addProperty("name", Name);
					request.addProperty("email", Email);
					request.addProperty("age", Age);
					request.addProperty("gender", Gender);
					request.addProperty("job", Job);
					request.addProperty("income", income);
					request.addProperty("interest", Interest);

					envelope.setOutputSoapObject(request);
					androidHttpTransportSE.call(Survey_SOAP_ACTION, envelope);

					SoapObject result = (SoapObject) envelope.bodyIn;
					Vector<Object> property2 = extracted(result);

					String[] results2 = null;

					for (int i = 0; i < property2.size(); i++) {

						results2 = new String[property2.size()];
						results2[i] = property2.elementAt(i).toString();
					}
				} catch (Exception ex) {

				}
			}
		};
		networkThread.start();
		UpdateAboutUs();
	}

	public void InsertOrder(final String Type, final String CName,
			final String Name, final String Address, final String Email,
			final String ContactNo, final String Remark) {
		Thread networkThread = new Thread() {
			@Override
			public void run() {

				try {
					SoapObject request = new SoapObject(NAMESPACE,
							Order_METHOD_NAME);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					HttpTransportSE androidHttpTransportSE = new HttpTransportSE(
							URL);

					request.addProperty("type", Type);
					request.addProperty("chinese_name", CName);
					request.addProperty("name", Name);
					request.addProperty("address", Address);
					request.addProperty("email", Email);
					request.addProperty("contact_no", ContactNo);
					request.addProperty("remark", Remark);

					envelope.setOutputSoapObject(request);
					androidHttpTransportSE.call(Order_SOAP_ACTION, envelope);

					SoapObject result = (SoapObject) envelope.bodyIn;
					Vector<Object> property2 = extracted(result);

					String[] results2 = null;

					for (int i = 0; i < property2.size(); i++) {

						results2 = new String[property2.size()];
						results2[i] = property2.elementAt(i).toString();
					}
				} catch (Exception ex) {
					success = false;
				}
			}
		};
		networkThread.start();
		success = true;
	}

	public void InsertEnquiry(final String EnquiryName,
			final String Enquiryemail, final String EnquirySuggestion,
			final String EnquiryRemark) {
		Thread networkThread = new Thread() {
			@Override
			public void run() {

				try {
					SoapObject request = new SoapObject(NAMESPACE,
							Enquiry_METHOD_NAME);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					HttpTransportSE androidHttpTransportSE = new HttpTransportSE(
							URL);

					request.addProperty("name", EnquiryName);
					request.addProperty("email", Enquiryemail);
					request.addProperty("suggestion", EnquirySuggestion);
					request.addProperty("remark", EnquiryRemark);

					envelope.setOutputSoapObject(request);
					androidHttpTransportSE.call(Enquiry_SOAP_ACTION, envelope);

					SoapObject result = (SoapObject) envelope.bodyIn;
					Vector<Object> property2 = extracted(result);

					String[] results2 = null;

					for (int i = 0; i < property2.size(); i++) {

						results2 = new String[property2.size()];
						results2[i] = property2.elementAt(i).toString();
					}
				} catch (Exception ex) {
					success = false;
				}
			}
		};
		networkThread.start();
		success = true;
	}

	public void InsertComment(final int CommentNewsID,
			final String CommentUserName, final String Comment,
			final int CommentApprovalStatus) {
		Thread networkThread = new Thread() {
			@Override
			public void run() {
				try {
					SoapObject request = new SoapObject(NAMESPACE,
							Comment_METHOD_NAME);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					HttpTransportSE androidHttpTransportSE = new HttpTransportSE(
							URL);

					request.addProperty("itemid", CommentNewsID);
					request.addProperty("name", CommentUserName);
					request.addProperty("comment", Comment);
					request.addProperty("approvalStatus", CommentApprovalStatus);

					envelope.setOutputSoapObject(request);
					androidHttpTransportSE.call(Comment_SOAP_ACTION, envelope);

					SoapObject result = (SoapObject) envelope.bodyIn;
					Vector<Object> property2 = extracted(result);

					String[] results2 = null;

					for (int i = 0; i < property2.size(); i++) {
						results2 = new String[property2.size()];
						results2[i] = property2.elementAt(i).toString();
					}

				} catch (Exception ex) {
					success = false;
				}
			}
		};
		networkThread.start();
		success = true;
	}

	public void InsertToken(final String name, final String deviceId,
			final String RegId) {
		Thread networkThread = new Thread() {
			@Override
			public void run() {
				try {
					SoapObject request = new SoapObject(NAMESPACE,
							insertTokenDevice_METHOD_NAME);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					HttpTransportSE androidHttpTransportSE = new HttpTransportSE(
							URL);

					request.addProperty("name", name);
					request.addProperty("devid", deviceId);
					request.addProperty("regid", RegId);

					envelope.setOutputSoapObject(request);
					androidHttpTransportSE.call(insertTokenDevice_SOAP_ACTION,
							envelope);

					SoapObject result = (SoapObject) envelope.bodyIn;
					Vector<Object> property2 = extracted(result);

					String[] results2 = null;

					for (int i = 0; i < property2.size(); i++) {
						results2 = new String[property2.size()];
						results2[i] = property2.elementAt(i).toString();
					}

				} catch (Exception ex) {
				}
			}
		};
		networkThread.start();
	}

	public void InsertFormSubmit(final int NewsSubmit, final int notification,
			final int sound, final int vibrate, final int popup, String regid) {
		try {
			List_UserDetails SubmitForm = new List_UserDetails(NewsSubmit,
					notification, sound, vibrate, popup, regid);

			dbhelper.AddSurveyForm(SubmitForm);
		} catch (Exception ex) {
		}

	}

	// ==========Check First Time User=============//
	public int CheckSurveySubmit() {
		int checkSubmit = 0;
		try {
			form = dbhelper.getSurveyFormSubmit();

			for (List_UserDetails nw : form) {
				if (nw.getFormSubmit() != 0) {
					checkSubmit = nw.getFormSubmit();
				}
			}
		} catch (Exception e) {
		}
		return checkSubmit;
	}

	// ===========Font Setting================//
	public void insertFontSize(int FontSize) {
		try {

			dbhelper.AddFontSize(FontSize);
		} catch (Exception ex) {
		}
	}

	public void UpdateFontSize(int FontSize) {
		try {

			dbhelper.updateFontSize(FontSize);
		} catch (Exception ex) {
		}
	}

	public int ReadFontSize() {
		int getfontsize = 15;
		try {
			getfontsize = dbhelper.getFontSize();

		} catch (Exception ex) {

		}
		return getfontsize;
	}
	
	public void DeleteFontSize(){
		try{
			dbhelper.DeleteFont();
		}catch(Exception ex){
			
		}
	}

	// =========Notification Setting=========//
	public void Updatenotification(int notification) {
		try {

			dbhelper.updatenotification(notification);
		} catch (Exception ex) {
		}
	}

	public int Readnotification() {
		int getnotification = 2;
		try {
			getnotification = dbhelper.getnotification();
		} catch (Exception ex) {
		}
		return getnotification;
	}

	public void Updatenotificationsound(int notification) {
		try {

			dbhelper.updatesound(notification);
		} catch (Exception ex) {
		}
	}

	public int Readnotificationsound() {
		int getnotification = 2;
		try {
			getnotification = dbhelper.getsound();
		} catch (Exception ex) {
		}
		return getnotification;
	}

	public void Updatenotificationvibrate(int notification) {
		try {
			dbhelper.updatevibration(notification);
		} catch (Exception ex) {
		}
	}

	public int Readnotificationvibrate() {
		int getnotification = 2;
		try {
			getnotification = dbhelper.getvibration();
		} catch (Exception ex) {
		}
		return getnotification;
	}

	public void Updatenotificationpopup(int notification) {
		try {
			dbhelper.updatepopup(notification);
		} catch (Exception ex) {
		}
	}

	public int Readnotificationpopup() {
		int getnotification = 2;
		try {
			getnotification = dbhelper.getpopup();
		} catch (Exception ex) {
		}
		return getnotification;
	}

	public void Updateregid(String regid) {
		try {
			dbhelper.updateregid(regid);
		} catch (Exception ex) {
		}
	}

	public String Readregid() {
		String getregid = "";
		try {
			getregid = dbhelper.getregid();
		} catch (Exception ex) {
		}
		return getregid;
	}

	// ==========SOAP Webservice=============//
	public List<List_News> RetrieveAllTopNewsFromSoap(SoapObject soap,
			int activitynumber) {

		news = new ArrayList<List_News>();
		Vector<Object> property2 = extracted(soap);

		for (int i = 0; i < property2.size(); i++) {
			SoapObject getPropertyD = (SoapObject) property2.get(i);

			String getPropertyID = getPropertyD.getProperty("id").toString();
			System.out.println(getPropertyID);
			String getCatID = getPropertyD.getProperty("catid").toString();
			String getPropertyTitle = getPropertyD.getProperty("title")
					.toString();
			System.out.println(getPropertyTitle);
			String getPropertyimagePathsave;

			String getPropertyimagePath = getPropertyD.getProperty("imagePath")
					.toString();
			getPropertyimagePathsave = getPropertyimagePath;

			String getPropertyIntrotext = getPropertyD.getProperty("introtext")
					.toString().replace("�C</p>", "�C\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�C�v</p>",
					"�C�v\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�H</p>", "�H\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�H�v</p>",
					"�H�v\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�I</p>", "�I\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�I�v</p>",
					"�I�v\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�G</p>", "�G\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�G�v</p>",
					"�G�v\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("��</p>", "��\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("���v</p>",
					"���v\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("</p>", "");
			getPropertyIntrotext = getPropertyIntrotext.replace("�C ", "�C");
			getPropertyIntrotext = getPropertyIntrotext.replace("<br />", "\n");

			String getPropertyFulltext = getPropertyD.getProperty("fulltext")
					.toString().replace("�C</p>", "�C\n");
			getPropertyFulltext = getPropertyFulltext.replace("�C�v</p>", "�C�v\n");
			getPropertyFulltext = getPropertyFulltext.replace("�H</p>", "�H\n");
			getPropertyFulltext = getPropertyFulltext.replace("�H�v</p>", "�H�v\n");
			getPropertyFulltext = getPropertyFulltext.replace("�I</p>", "�I\n");
			getPropertyFulltext = getPropertyFulltext.replace("�I�v</p>", "�I�v\n");
			getPropertyFulltext = getPropertyFulltext.replace("�G</p>", "�G\n");
			getPropertyFulltext = getPropertyFulltext.replace("�G�v</p>", "�G�v\n");
			getPropertyFulltext = getPropertyFulltext.replace("��</p>", "��\n");
			getPropertyFulltext = getPropertyFulltext.replace("���v</p>", "���v\n");
			getPropertyFulltext = getPropertyFulltext.replace("</p>", "");
			getPropertyFulltext = getPropertyFulltext.replace("<br />", "\n");

			String getauthor = getPropertyD.getProperty("author").toString();
//			for (int j = 0; j < getauthor.length(); j++) {
//				if (Character.isWhitespace(getauthor.charAt(j))) {
//					getauthor = getauthor.substring(0, j - 1);
//					break;
//				}
//			}
			String getPropertyArticleDate = getPropertyD
					.getProperty("articleDate").toString().substring(0, 10);
			List_News insertNews = new List_News(
					Integer.parseInt(getPropertyID),
					Integer.parseInt(getCatID), getPropertyTitle,
					getPropertyIntrotext, getPropertyFulltext,
					getPropertyArticleDate, getPropertyimagePathsave, getauthor);
			
			dbhelper.AddNews(insertNews);

			List_News addnews = new List_News();
			addnews.SetID(Integer.parseInt(getPropertyD.getProperty("id")
					.toString()));
			addnews.SetCatID(Integer.parseInt(getPropertyD.getProperty("catid")
					.toString()));
			addnews.setNtitle(getPropertyD.getProperty("title").toString());
			addnews.setImagePaht(getPropertyD.getProperty("imagePath")
					.toString());
			addnews.setNintroText(getPropertyD.getProperty("introtext")
					.toString());
			addnews.setFullText(getPropertyD.getProperty("fulltext").toString());
			addnews.setNArtical(getPropertyD.getProperty("articleDate")
					.toString());
			addnews.setAuthor(getPropertyD.getProperty("author").toString());

			news.add(addnews);
		}
		if (activitynumber == 1) {
			Intent intent = new Intent(activity, Main_AllLatestNews.class);
			bundle = new Bundle();
			bundle.putBoolean("isshow", false);
			bundle.putBoolean("needdownload", false);
			bundle.putBoolean("isfirsttime", false);
			bundle.putInt("activitynumber", activitynumber);
			bundle.putInt("position", 0);
			intent.putExtras(bundle);
			activity.startActivity(intent);
			activity.finish();
		} else if (activitynumber == 6) {
			Intent intent = new Intent(activity,
					Main_ParticularNewsDetail.class);
			bundle = new Bundle();
			bundle.putString("newsid", newsid + "");
			bundle.putInt("previousactivitynumber", activitynumber);
			bundle.putInt("activitynumber", 5);
			intent.putExtras(bundle);
			activity.startActivity(intent);
			activity.finish();
		}
		return news;
	}

	public List<List_News> RetrieveMoreCatNewsFromSoap(SoapObject soap,
			int CatID) {
		List<List_News> newNews = new ArrayList<List_News>();
		Vector<Object> property2 = extracted(soap);

		for (int i = 0; i < property2.size(); i++) {
			SoapObject getPropertyD = (SoapObject) property2.get(i);
			String getPropertyID = getPropertyD.getProperty("id").toString();
			String getPropertyTitle = getPropertyD.getProperty("title")
					.toString();
			String getPropertyimagePathsave;

			String getPropertyimagePath = getPropertyD.getProperty("imagePath")
					.toString();
			getPropertyimagePathsave = getPropertyimagePath;

			String getPropertyIntrotext = getPropertyD.getProperty("introtext")
					.toString().replace("�C</p>", "�C\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�C�v</p>",
					"�C�v\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�H</p>", "�H\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�H�v</p>",
					"�H�v\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�I</p>", "�I\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�I�v</p>",
					"�I�v\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�G</p>", "�G\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("�G�v</p>",
					"�G�v\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("��</p>", "��\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("���v</p>",
					"���v\n");
			getPropertyIntrotext = getPropertyIntrotext.replace("</p>", "");
			getPropertyIntrotext = getPropertyIntrotext.replace("�C ", "�C");
			getPropertyIntrotext = getPropertyIntrotext.replace("<br />", "\n");

			String getPropertyFulltext = getPropertyD.getProperty("fulltext")
					.toString().replace("�C</p>", "�C\n");
			getPropertyFulltext = getPropertyFulltext.replace("�C�v</p>", "�C�v\n");
			getPropertyFulltext = getPropertyFulltext.replace("�H</p>", "�H\n");
			getPropertyFulltext = getPropertyFulltext.replace("�H�v</p>", "�H�v\n");
			getPropertyFulltext = getPropertyFulltext.replace("�I</p>", "�I\n");
			getPropertyFulltext = getPropertyFulltext.replace("�I�v</p>", "�I�v\n");
			getPropertyFulltext = getPropertyFulltext.replace("�G</p>", "�G\n");
			getPropertyFulltext = getPropertyFulltext.replace("�G�v</p>", "�G�v\n");
			getPropertyFulltext = getPropertyFulltext.replace("��</p>", "��\n");
			getPropertyFulltext = getPropertyFulltext.replace("���v</p>", "���v\n");
			getPropertyFulltext = getPropertyFulltext.replace("</p>", "");
			getPropertyFulltext = getPropertyFulltext.replace("<br />", "\n");

			String getauthor = getPropertyD.getProperty("author").toString();
			for (int j = 0; j < getauthor.length(); j++) {
				if (Character.isWhitespace(getauthor.charAt(j))) {
					getauthor = getauthor.substring(0, j - 1);
					break;
				}
			}
			String getPropertyArticleDate = getPropertyD
					.getProperty("articleDate").toString().substring(0, 10);

			List_News insertNews = new List_News(
					Integer.parseInt(getPropertyID), CatID, getPropertyTitle,
					getPropertyIntrotext, getPropertyFulltext,
					getPropertyArticleDate, getPropertyimagePathsave, getauthor);
			dbhelper.AddNews(insertNews);

			List_News addnews = new List_News();
			addnews.SetID(Integer.parseInt(getPropertyD.getProperty("id")
					.toString()));
			addnews.SetCatID(CatID);
			addnews.setNtitle(getPropertyD.getProperty("title").toString());
			addnews.setImagePaht(getPropertyD.getProperty("imagePath")
					.toString());
			addnews.setNintroText(getPropertyD.getProperty("introtext")
					.toString());
			addnews.setFullText(getPropertyD.getProperty("fulltext").toString());
			addnews.setNArtical(getPropertyD.getProperty("articleDate")
					.toString());
			addnews.setAuthor(getPropertyD.getProperty("author").toString());

			newNews.add(addnews);
		}
		Intent intent = new Intent(activity,
				Main_ParticularCategoryAllNews.class);
		bundle = new Bundle();
		bundle.putInt("catnewsid", catnewsid);
		bundle.putString("pagetitle", pagetitletext);
		bundle.putInt("position", position);
		bundle.putBoolean("isshow", false);
		bundle.putInt("scrollposition", index);
		intent.putExtras(bundle);
		activity.startActivity(intent);
		activity.finish();
		return newNews;
	}

	@SuppressWarnings("unchecked")
	public static Vector<Object> extracted(SoapObject soap) {
		return (Vector<Object>) soap.getProperty(0);
	}
}
